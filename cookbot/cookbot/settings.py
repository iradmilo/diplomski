# Scrapy settings for cookbot project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#
ITEM_PIPELINES = {
    #'cookbot.pipelines.DuplicatesPipeline': 300,
}

BOT_NAME = 'cookbot'

SPIDER_MODULES = ['cookbot.spiders']
NEWSPIDER_MODULE = 'cookbot.spiders'

#SPIDER_MIDDLEWARES = {
#    'scrapy.spidermiddlewares.depth.DepthMiddleware': 100
#}

#DEPTH_PRIORITY = -10
# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'cookbot ( +http://mrorii.github.io/)'
