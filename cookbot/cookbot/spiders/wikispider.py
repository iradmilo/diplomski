#!/usr/bin/env python

import os
import re
import urlparse

from scrapy import Request, FormRequest
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector, XmlXPathSelector
from scrapy.linkextractors import LinkExtractor

from cookbot.items import wikiIngredients, wikiIngredient

#see allowed an disallowed domains with base url + robots.txt

class WikiSpider(CrawlSpider):

    name = 'wikispider'
    allowed_domains = ['en.wikibooks.org']
    download_delay = 1
    nutr_url_ext = '/wiki/'

    start_urls = ['https://en.wikibooks.org/wiki/Cookbook:Ingredients']
   
    rules = (

        Rule(LinkExtractor(allow=('https://en.wikibooks.org/wiki/Cookbook:Ingredients', ), 
                            deny=('/w/, /api/, /trap/, /wiki/Special:, /wiki/Spezial:, /wiki/Spesial:, /wiki/Special%3A, /wiki/Spezial%3A, /wiki/Spesial%3A')
             ), callback='parse_ingredients'),
    )

    def parse_ingredients(self, response):

        hxs = HtmlXPathSelector(response)
        
        ingredients = wikiIngredients()

        ingredient_list = []
       
        try:
            lis = hxs.select('//div[@id="mw-content-text"]/div/ul/li')
            for res in lis.select('.//a/text()').extract():
                ingredient = wikiIngredient()
                ingredient['ingredient'] = res.encode('utf-8')
                ingredient_list.append(ingredient) 
        except:
            pass
        
        ingredients['ingredients'] = ingredient_list

        print(ingredients)

        return ingredients
        