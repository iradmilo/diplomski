#!/usr/bin/env python

   # International Recipes
# 'https://allrecipes.com/recipes/world-cuisine/african/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/chinese/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/japanese/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/korean/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/indian/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/thai/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/eastern-european/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/french/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/german/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/greek/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/italian/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/middle-eastern/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/latin-american/mexican/main.aspx',
# 'https://allrecipes.com/recipes/USA-Regional-and-Ethnic/Cajun-and-Creole/main.aspx',
# 'https://allrecipes.com/recipes/USA-Regional-and-Ethnic/Southern-Recipes/Southern-Cooking-by-State/main.aspx',

   # American Recipes
# 'https://allrecipes.com/recipes/main-dish/main.aspx',
# 'https://allrecipes.com/recipes/92/meat-and-poultry/main.aspx',
#
# 'https://allrecipes.com/recipes/1116/fruits-and-vegetables/main.aspx',
# 'https://allrecipes.com/recipes/93/seafood/main.aspx',
# 'https://allrecipes.com/recipes/95/pasta/main.aspx',

# # By Meal Part
##num above link -> number of pages worth parsing by category(meal part)
##y next to number -> note if domain is parsed
#240-y
# 'https://allrecipes.com/recipes/76/appetizers-and-snacks/main.aspx',
#154-y
# 'https://allrecipes.com/recipes/77/drinks/main.aspx',
#120-y
# 'https://allrecipes.com/recipes/78/breakfast-and-brunch/main.aspx',
#11-n
# 'https://allrecipes.com/recipes/260/main-dish/salads/main.aspx',
#226-y
# 'https://allrecipes.com/recipes/94/soups-stews-and-chili/main.aspx',
#850-y
# 'https://allrecipes.com/recipes/80/main-dish/main.aspx',
#221-y
# 'https://allrecipes.com/recipes/81/side-dish/main.aspx',
#150-y
# 'https://allrecipes.com/recipes/156/bread/main.aspx',
#650-y
# 'https://allrecipes.com/recipes/79/desserts/main.aspx',

starting_domains = [
    'https://allrecipes.com/recipes/76/appetizers-and-snacks/?page=',
    241,
    'https://allrecipes.com/recipes/77/drinks/?page=',
    155,
    'https://allrecipes.com/recipes/78/breakfast-and-brunch/?page=',
    121,
    'https://allrecipes.com/recipes/260/main-dish/salads/?page=',
    12,
    'https://allrecipes.com/recipes/94/soups-stews-and-chili/?page=',
    227,
    'https://allrecipes.com/recipes/80/main-dish/?page=',
    851,
    'https://allrecipes.com/recipes/81/side-dish/?page=',
    222,
    'https://allrecipes.com/recipes/156/bread/?page=',
    151,
    'https://allrecipes.com/recipes/79/desserts/?page=',
    651
]

import os
import re
import urlparse

from scrapy import Request, FormRequest
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector, XmlXPathSelector
from scrapy.linkextractors import LinkExtractor

from cookbot.items import AllrecipesRecipe, Ingredient, Nutrient

class AllrecipesSpider(CrawlSpider):
    name = 'allrecipes_extended'
    allowed_domains = ['allrecipes.com']
    download_delay = 1

    start_urls = []

    for counter in range(0,len(starting_domains),2):
        start_urls += [starting_domains[counter]+str(i) for i in range(int(starting_domains[counter+1]))]


    rules = (
        Rule(LinkExtractor(allow=('recipes/.+/main\.aspx\?page=\d+(#)?\d*', ), deny=('/admin/', '/account/', '/personal-recipe/'\
        '/recipe-tools/', '/search/', '/custom-recipe/', '/new-this-month/', '/content/partner/', '/featured/', '/cook/my/'))),
        Rule(LinkExtractor(allow=('/recipe/', )), callback='parse_recipe'),
    )

    def parse_recipe(self, response):

        hxs = HtmlXPathSelector(response)

        recipe = AllrecipesRecipe()

        # name
        try:
            recipe['name'] = hxs.select("//h1[@itemprop='name']/text()")[0].extract().strip()
        except:
            recipe['name'] = None


        #prep_time
        try:
            time = hxs.select("//time[@itemprop='prepTime']/span/span/text()").extract()
            units = hxs.select("//time[@itemprop='prepTime']/span/text()").extract()
            recipe['prep_time'] = ''
            for counter, i in enumerate(time):
                recipe['prep_time'] +=  time[counter] + units[counter]
            if recipe['prep_time'] == '':
                recipe['prep_time'] = None
        except:
            recipe['prep_time'] = None

        #cook_time
        try:
            time = hxs.select("//time[@itemprop='cookTime']/span/span/text()").extract()
            units = hxs.select("//time[@itemprop='cookTime']/span/text()").extract()
            recipe['cook_time'] = ''
            for counter, i in enumerate(time):
                recipe['cook_time'] +=  time[counter] + units[counter]
            if recipe['cook_time'] == '':
                recipe['cook_time'] = None
        except:
            recipe['cook_time'] = None

        #ratings_number

        try:
            num_ratings = hxs.select("//h4[@class='helpful-header']/text()").extract()[0]
            recipe['num_ratings'] = [int(s) for s in num_ratings.split() if s.isdigit()][0]

        except:
            recipe['num_ratings'] = None

        yield recipe