#!/usr/bin/env python

   # International Recipes
# 'https://allrecipes.com/recipes/world-cuisine/african/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/chinese/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/japanese/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/korean/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/indian/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/asian/thai/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/eastern-european/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/french/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/german/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/greek/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/european/italian/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/middle-eastern/main.aspx',
# 'https://allrecipes.com/recipes/world-cuisine/latin-american/mexican/main.aspx',
# 'https://allrecipes.com/recipes/USA-Regional-and-Ethnic/Cajun-and-Creole/main.aspx',
# 'https://allrecipes.com/recipes/USA-Regional-and-Ethnic/Southern-Recipes/Southern-Cooking-by-State/main.aspx',

   # American Recipes
# 'https://allrecipes.com/recipes/main-dish/main.aspx',
# 'https://allrecipes.com/recipes/92/meat-and-poultry/main.aspx',
#
# 'https://allrecipes.com/recipes/1116/fruits-and-vegetables/main.aspx',
# 'https://allrecipes.com/recipes/93/seafood/main.aspx',
# 'https://allrecipes.com/recipes/95/pasta/main.aspx',

# # By Meal Part
##num above link -> number of pages worth parsing by category(meal part)
##y next to number -> note if domain is parsed
#240-y
# 'https://allrecipes.com/recipes/76/appetizers-and-snacks/main.aspx',
#154-y
# 'https://allrecipes.com/recipes/77/drinks/main.aspx',
#120-y
# 'https://allrecipes.com/recipes/78/breakfast-and-brunch/main.aspx',
#11-n
# 'https://allrecipes.com/recipes/260/main-dish/salads/main.aspx',
#226-y
# 'https://allrecipes.com/recipes/94/soups-stews-and-chili/main.aspx',
#850-y
# 'https://allrecipes.com/recipes/80/main-dish/main.aspx',
#221-y
# 'https://allrecipes.com/recipes/81/side-dish/main.aspx',
#150-y
# 'https://allrecipes.com/recipes/156/bread/main.aspx',
#650-y
# 'https://allrecipes.com/recipes/79/desserts/main.aspx',

import os
import re
import urlparse

from scrapy import Request, FormRequest
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector, XmlXPathSelector
from scrapy.linkextractors import LinkExtractor

from cookbot.items import AllrecipesRecipe, Ingredient, Nutrient

class AllrecipesSpider(CrawlSpider):
    name = 'allrecipes'
    allowed_domains = ['allrecipes.com']
    download_delay = 1
    nutr_url_ext = 'fullrecipenutrition/'
    #nutr_words = {}

    start_urls = []

    for i in range(11):
        address='https://allrecipes.com/recipes/260/main-dish/salads/?page='+str(i)
        start_urls.append(address)

    rules = (
        Rule(LinkExtractor(allow=('recipes/.+/main\.aspx\?page=\d+(#)?\d*', ), deny=('/admin/', '/account/', '/personal-recipe/'\
        '/recipe-tools/', '/search/', '/custom-recipe/', '/new-this-month/', '/content/partner/', '/featured/', '/cook/my/'))),
        Rule(LinkExtractor(allow=('/recipe/', )), callback='parse_recipe'),
    )

    def parse_recipe(self, response):

        hxs = HtmlXPathSelector(response)

        recipe = AllrecipesRecipe()

        # name
        try:
            recipe['name'] = hxs.select("//h1[@itemprop='name']/text()")[0].extract().strip()
        except:
            recipe['name'] = None

        #category
        try:
            recipe['category'] = hxs.select("//span[@itemprop='name']/text()")[-1].extract().strip()
        except:
            recipe['category'] = None

        # author
        try:
            recipe['author'] = hxs.select("//span[@itemprop='author']/text()")[0].extract()
        except:
            recipe['author'] = None

        # description
        des = hxs.select("//div[@itemprop='description']/text()")[0].extract()
        des = des.replace('\r', '').replace('\"', '')
        des = " ".join(des.split())
        recipe['description'] = des

        # rating
        try:
            recipe['rating'] = float(
                hxs.select("//meta[@itemprop='ratingValue']/@content").extract()[0]
            )
        except:
            recipe['rating'] = None

        #ready_in_time
        try:
            recipe['ready_in_time'] = hxs.select("//span[@class='ready-in-time']/text()").extract()[0]
        except:
            recipe['ready_in_time'] = None

        #prep_time
        try:
            time = hxs.select("//time[@itemprop='prepTime']/span/span/text()").extract()
            print(time)
            units = hxs.select("//time[@itemprop='prepTime']/span/text()").extract()
            print(units)
            recipe['prep_time'] = ''
            for counter, i in enumerate(time):
                recipe['prep_time'] +=  time[counter] + units[counter]
            if recipe['prep_time'] == '':
                recipe['prep_time'] = None
            print("Prep time: ", recipe['prep_time'])
        except:
            recipe['prep_time'] = None

        #cook_time
        try:
            time = hxs.select("//time[@itemprop='cookTime']/span/span/text()").extract()
            print(time)
            units = hxs.select("//time[@itemprop='cookTime']/span/text()").extract()
            print(units)
            recipe['cook_time'] = ''
            for counter, i in enumerate(time):
                recipe['cook_time'] +=  time[counter] + units[counter]
            if recipe['cook_time'] == '':
                recipe['cook_time'] = None
            print("Cook time: ", recipe['cook_time'])
        except:
            recipe['cook_time'] = None

        #servings
        try:
            recipe['servings'] = int(
                hxs.select("//meta[@itemprop='recipeYield']/@content").extract()[0]
            )
        except:
            recipe['servings'] = None

        #calories
        try:
            recipe['calories'] = int(
                hxs.select("//span[@class='calorie-count']/span/text()").extract()[0]
            )
        except:
            recipe['calories'] = None

        try:
            recipe['num_reviews'] = int(
                hxs.select("//span[@class='recipe-reviews__header--count']/text()").extract()[0]
            )
        except:
            recipe['num_reviews'] = None

        #instructions

        instructions = hxs.select("//li[@class='step']/span/text()").extract()
        recipe['instructions'] = [instruction.strip() for instruction in instructions]

        # ingredients
        recipe['ingredients'] = hxs.select("//span[@itemprop='ingredients']/text()").extract()

        #nutrients
        recipe['url'] = response.url
        url = response.url + self.nutr_url_ext
        request = Request(url, callback=self.parse_nutrients, dont_filter=True)
        request.meta['recipe'] = recipe
        print(response.url)
        yield request

    def parse_nutrients(self, response):

        hxs = HtmlXPathSelector(response)
        recipe = response.meta['recipe']

        nutrients = []
        nutrient_nodes = hxs.select("//div[@class='nutrition-row']")
        for nutrient_node in nutrient_nodes:
            try:
                name = nutrient_node.select("span[@class='nutrient-name']/text()") \
                                    .extract()[0]
                value = nutrient_node.select("span/span[@class='nutrient-value']/text()") \
                                    .extract()[0]
                try:
                    daily_value = nutrient_node.select("span[@class='daily-value']/text()") \
                                    .extract()[0]
                except:
                    daily_value = None
            except:
                try:
                    name = nutrient_node.select("span[@class='nutrient-name indent']/text()") \
                                        .extract()[0]
                    value = nutrient_node.select("span/span[@class='nutrient-value']/text()") \
                                        .extract()[0]
                    try:
                        daily_value = nutrient_node.select("span[@class='daily-value']/text()") \
                                        .extract()[0]
                    except:
                        daily_value = None
                except:
                    pass

            nutrient = Nutrient()
            nutrient['nutrient_name'] = name
            nutrient['nutrient_value'] = value
            nutrient['daily_value'] = daily_value

            nutrients.append(nutrient)

        recipe['nutrients'] = nutrients


        yield recipe
