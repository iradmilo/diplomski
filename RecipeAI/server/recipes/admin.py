from django.contrib import admin

# Register your models here.
from .models import Recipe, IngredientContent, RecipeIngredient, Ingredient

admin.site.register(Recipe)
admin.site.register(IngredientContent)
admin.site.register(Ingredient)
admin.site.register(RecipeIngredient)