# Generated by Django 2.0.5 on 2018-05-31 15:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_remove_ingredient_recipes'),
    ]

    operations = [
        migrations.AddField(
            model_name='ingredient',
            name='recipes',
            field=models.ManyToManyField(through='recipes.RecipeIngredient', to='recipes.Recipe'),
        ),
    ]
