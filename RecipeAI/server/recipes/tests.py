import logging
from timeit import default_timer as timer
from django.db import IntegrityError
import re
import numba
from numba import jit
from math import sqrt

logger = logging.getLogger('recipes')
logger.setLevel(logging.DEBUG)

from django.test import TestCase
from .models import Recipe, Category, Instruction, RecipeInstruction,\
    Nutrient, Recipe, RecipeNutrient, IngredientType, IngredientCategory,\
    IngredientSubcategory, IngredientContent, IngredientList, CategorySubcategory,\
    Ingredient

# Create your tests here.


'''Insertion tests'''
class RecipeInsertTestCase(TestCase):
    def setUp(self):
        try:
            category = Category.objects.create(category="Main Dish")
            recipe = Recipe.objects.create(recipe="Onion",\
                        category=category, rating=1.5, reviews=100, servings=10)
         
        except IntegrityError as e:
            logger.error('Exception message '+ str(e))

    def test_insertion(self):
        recipe = Recipe.objects.get(recipe="Onion")
        
        self.assertEqual(recipe.recipe, "Onion")
        self.assertEqual(recipe.reviews, 100)
    

'''Checking exception tests'''
class RecipeExcepionsTestCase(TestCase):
    def setUp(self): 
        try:
            category = Category.objects.create(category="Main Dish")
            recipe = Recipe.objects.create(recipe="Onion",\
                        category=category, rating=1.5, reviews=100, servings=10)
        except IntegrityError as e:
            logger.error('Exception message '+ str(e))

    def test_exception_raised(self):
        recipe = Recipe.objects.get(recipe="Onion")
        category1 = Category.objects.create(category="Side Dish")

        self.assertRaises(Recipe.DoesNotExist, Recipe.objects.get, recipe__exact = "Ante")
        self.assertRaises(IntegrityError, Recipe.objects.get_or_create,\
                    recipe="Onion", category=category1, rating=1.5, reviews=100, servings=10)

'''Count last step'''
class RecipeStepsTest(TestCase):
    step = 1
    def setUp(self):
        instructions = ['a', 'b', '', 'c']
    
        try:
            category = Category.objects.create(category="Main Dish")
            recipe = Recipe.objects.create(recipe="Onion",\
                            category=category, rating=1.5, reviews=100, servings=10)
            
            for instruction in instructions:            
                if instruction == "":
                    continue
                else:
                    Instruction.objects.get_or_create(instruction=instruction)
                    inst = Instruction.objects.get(instruction=instruction)
                    try:                 
                        RecipeInstruction.objects.get_or_create(recipe=recipe,\
                                                                instruction=inst,\
                                                                step=self.step)
                    except IntegrityError as e:
                        logger.error('Failed at inserting recipe instruction'+ str(e))
                    self.step += 1                                                          
                    
        
        except IntegrityError as e:
            logger.error('Exception message '+ str(e))

    '''test updating recipe'''
    def test_update_recipe(self):
        recipe = Recipe.objects.get(recipe="Onion")
        '''number of steps pre update'''
        steps_pre = recipe.instruction_steps_total

        last_recipe_instruction = RecipeInstruction.objects.filter(recipe=recipe.pk).last()
        Recipe.objects.update(recipe="Onion", instruction_steps_total=last_recipe_instruction.step)

        self.assertEqual(steps_pre, 0)
        self.assertEqual(Recipe.objects.get(recipe="Onion").\
             instruction_steps_total, last_recipe_instruction.step)

    def test_finding_step_value(self):     
        try:    
            recipe = Recipe.objects.get(recipe="Onion")
            last_recipe_instruction = RecipeInstruction.objects.filter(recipe=recipe.pk).last()
        except ValueError as ve:
            print(ve)
        
        '''subtracting 1 because of last addition in insertion'''
        self.assertEqual(last_recipe_instruction.step, self.step - 1)


# '''Test inserting ingredients'''
# class IngredientTest(TestCase):
#     ingr = 'Almond'
#     content = 'almond cake'

#     def findIngredient(self, ingredient, sentence):
#         return re.search(r'\b({0})(s|es)?\b'.format(ingredient), sentence, flags=re.IGNORECASE)

#     def setUp(self):
#         try:
#             start = timer()
#             ingr_category = 'Category'
#             ingr_subcategory = 'Subcategory'
#             recipe_name = 'Onion'
#             t = 'Type 1'
#             typeI = IngredientType.objects.create(type_name=t)
#             category = Category.objects.create(category="Main Dish")
#             recipe = Recipe.objects.create(recipe=recipe_name,\
#                             category=category, rating=1.5, reviews=100, servings=10)

#             ingr_cat = IngredientCategory.objects.create(category=ingr_category)
#             ingr_sub = IngredientSubcategory.objects.create(subcategory=ingr_subcategory)
#             cat_sub = CategorySubcategory.objects.create(\
#                     ingredient_category=IngredientCategory.objects.get(\
#                         category=ingr_category),\
#                     ingredient_subcategory=IngredientSubcategory.objects.get(\
#                         subcategory=ingr_subcategory)
#                 )                                                      
#             ingredient = Ingredient.objects.create(\
#                     ingredient=self.ingr,\
#                     category_subcategory=cat_sub,
#                     ingredient_type=typeI
#                 )
#             ingredient_content = IngredientContent.objects.create(content=self.content) 
#             if self.findIngredient(ingredient.ingredient, self.content):
#                 IngredientList.objects.get_or_create(\
#                     recipe=Recipe.objects.get(recipe=recipe),
#                     ingredient_content=ingredient_content
#                 )

#             end = timer()
#         except IntegrityError as e:
#             logger.error('Exception message '+ str(e))

#         print(end - start)

#     def test_finding_step_value(self):     
#         try:    
#             recipe = Recipe.objects.get(recipe="Onion")
#             ingredient = Ingredient.objects.get(ingredient=self.ingr)
#             ingredient_list = IngredientList.objects.get(ingredient=ingredient)
#         except ValueError as ve:
#             print(ve)         
        
#         '''subtracting 1 because of last addition in insertion'''
#         self.assertEqual(ingredient_list.ingredient_content.content, 'almond cake')

""" testing numba optimizing library """
class testNumba(TestCase):
    def setUp(self):
        self.NUMBER = 10000

    def function_core(self,number):
        num = 0
        for i in range(2, number):
            k = 0
            for j in range(2, i - 1):
                if i % j is 0:
                    k = 1
                    break
            if k == 0:
                num += 1
        print(num)

    @jit
    def calculate_all_prime_nums(self, number):
        self.function_core(number)

            
    def calculate_all_prime_nums_without(self, number):
        self.function_core(number)

    def test_with_numba(self):
        start = timer()
        self.calculate_all_prime_nums(self.NUMBER)
        stop = timer()
        print("Time with numba", stop-start)

    def test_without_numba(self):
        start = timer()
        self.calculate_all_prime_nums_without(self.NUMBER)
        stop = timer()
        print("Time without numba", stop-start)
        
#class nutrientPresentQuery