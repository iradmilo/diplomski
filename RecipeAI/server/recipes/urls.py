from django.urls import path, include
from . import views
from django.views.generic.base import RedirectView
#path('', RedirectView.as_view(pattern_name='index', permanent=False)),

urlpatterns = [
    path('ingredients/', views.serve_ingredients, name='serve_ingredients'),
    path('recipes/', views.index, name='index'),
]