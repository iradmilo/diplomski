import re
import json
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.http import Http404
from pprint import pprint
from django.db import IntegrityError
from .models import Recipe, Ingredient, RecipeNutrient, RecipeInstruction, IngredientList, Category
from timeit import default_timer as timer

REQUESTED_NUMBER = 70

class RecipeRequest:

    '''
    @param ingredient Ingredient from base to if its in ingedient_list
    @param ingredient_string Ingredient_string is concatenated text of ingredient description texts
    '''
    def regular_expression(self, ingredient, ingredient_string):
        return re.search(r'\b({0})(es|s)?\b'.format(ingredient), ingredient_string, flags=re.IGNORECASE)

    '''
    @param ingredients Ingredients to check for
    @param ingredient_list List of raw(description) text ingredients(ex. '2 spoons of brown sugar')
    '''
    def findIngredients(self, ingredients, ingredient_list):
        present_ingredients = []
        ingredient_string = ' '.join(ingredient_list)
        for ingredient in ingredients:
            if self.regular_expression(ingredient=ingredient, ingredient_string=ingredient_string):
                present_ingredients.append(ingredient)
        return present_ingredients

    '''
    @param fat_per percentage of fat in meal
    @param carb_per percentage of carbohydrates in meal
    @param protein_per percentage of protein in meal
    @return macronutrient balance score
    '''
    def calculate_macronutrient_balace(self, fat_per, carb_per, protein_per):
        balance = 0
        if fat_per >= 25 and fat_per <= 35:
            balance += 1
        if carb_per >= 45 and carb_per <= 65:
            balance += 1
        if protein_per >= 10 and protein_per <= 35:
            balance += 1
        return balance

    '''
    @param ingredients Ingredients requested
    @param category Category to search recipes by
    @param requested number of recipes starting from zero
    @param from_number Number of index in dict from
    @param to_number Number of index in dict to
    '''
    def get_request_data(self, ingredients, category, requested_number, from_number, to_number ):

        responseObject = []
        final_recipe_instructions = []
        final_recipe_nutrients = []
        recipeArray = []
        dictRecipes = {}
        valDividedRecipes = {}

        print('Ingredients', ingredients)
        ingr_list_length = len(ingredients)

        '''Mysql can use only 61 tables in join'''
        if ingr_list_length >= 60:
            ingr_list_length = 60

        try:
            inter_list = []
            if category == "All":
                recipes = Ingredient.objects.get(ingredient=ingredients[0]).recipes.all()
            else:
                recipes = Ingredient.objects.get(ingredient=ingredients[0]).\
                            recipes.filter(category=Category.objects.get(category=category))
     
            inter_list.append(recipes)
            for i in range(1, ingr_list_length):
                try:
                    if category == "All":
                        recipesAdd = Ingredient.objects.get(ingredient=ingredients[i]).recipes.all()
                    else:
                        recipesAdd = Ingredient.objects.get(ingredient=ingredients[i]).\
                            recipes.filter(category=Category.objects.get(category=category))
                except Ingredient.DoesNotExist as dne:
                    print(str(dne))
                    raise Http404("Ingredient does not exist")
                inter_list.append(recipesAdd)
                recipes |= recipesAdd

            '''dict for asserting number of times required ingredients show in specific recipe'''
            for recipe in recipes:
                try:
                    dictRecipes[recipe.pk] += 1
                except KeyError: 
                    dictRecipes.update({recipe.pk: 1})

            dict_len = len(dictRecipes)

            if requested_number != 0:
                from_number = 0
                to_number = requested_number
                if to_number > dict_len:
                    to_number = dict_len
            else:
                try:
                    if from_number > dict_len:
                        from_number = dict_len - 10 

                    if to_number > dict_len:
                        to_number = dict_len
                except:
                    print('Error with requested to-from numbers')
            

            recipes = list()
            counter = ingr_list_length
            while len(recipes) < to_number and counter > 0:
                valDividedRecipes[counter] = Recipe.objects.filter(pk__in =
                                            [k for k,v in dictRecipes.items() if v == counter])

                '''reverse == descending order'''
                for recipe in valDividedRecipes[counter].order_by('score', 'nrf').reverse():
                    if len(recipes) > to_number:
                        break
                    if recipe not in recipes:
                        recipes.append(recipe)
                counter -= 1

            recipes = recipes[from_number:to_number]

            recipe_nutrients = [RecipeNutrient.objects.filter(recipe=recipe) for recipe in recipes]

            recipe_instructions = [[instructions\
                for instructions in RecipeInstruction.objects.filter(recipe=recipe)]\
                for recipe in recipes]

            for instructions in recipe_instructions:
                instr_Object = {}
                for instruction in instructions:
                    instr_Object.update({instruction.step: instruction.instruction.instruction})
                final_recipe_instructions.append(instr_Object)

            for nutrient_list in recipe_nutrients:
                recipe_nutrients = []
                for nutrient in nutrient_list:
                    
                    recipe_nutrients.append(
                        {   
                            'values': {
                                'daily_value':nutrient.daily_value,
                                'value':nutrient.nutrient_value
                            },
                            'nutrient': nutrient.nutrient.nutrient
                        }
                    )
                final_recipe_nutrients.append(recipe_nutrients)

            for counter, recipe in enumerate(recipes[from_number:to_number]):
                ingredient_list = [content.content for content in recipe.ingredient_list.all()]
                ingredients_present = self.findIngredients(ingredients, ingredient_list)
                macronutrient_balace = self.calculate_macronutrient_balace(fat_per=recipe.fat_per, \
                carb_per=recipe.carb_per, \
                protein_per=recipe.protein_per)

                responseObject.append({
                    'ingredient_list':ingredient_list,
                    'instructions':final_recipe_instructions[counter],
                    'nutrients':final_recipe_nutrients[counter],
                    'total_steps': recipe.instruction_steps_total,
                    'recipe':recipe.recipe,
                    'category':recipe.category.category,
                    'nr':recipe.nr,
                    'lim3':recipe.lim3,
                    'nrf':recipe.nrf,
                    'url':recipe.url,
                    'score':recipe.score,
                    'prep_time':recipe.prep_time,
                    'cook_time':recipe.cook_time,
                    'ready_time':recipe.ready_time,
                    'calories':recipe.calories,
                    'servings':recipe.servings,
                    'carb_per':recipe.carb_per,
                    'fat_per':recipe.fat_per,
                    'protein_per':recipe.protein_per,
                    'total_per':recipe.total_per,
                    'ingredients_in_recipe':ingredients_present,
                    'ingredients_in_recipe_num': len(ingredients_present),
                    'macronutrient_balance': macronutrient_balace,
                    'reviews': recipe.reviews
                })

            return responseObject

        except Ingredient.DoesNotExist as dne:
            print(str(dne))
            raise Http404("Ingredient does not exist")

def handle_request_numbers(request):

    """requested recipes"""
    try:
        requested_number = int(request.GET.get('number'))
    except TypeError:
        requested_number = 0

    """number of recipes from and to"""
    try:
        from_number = int(request.GET.get('from'))
    except TypeError:
        from_number = 0

    try:
        to_number = int(request.GET.get('to'))
    except TypeError:
        to_number = REQUESTED_NUMBER

    if requested_number < 0 or requested_number > REQUESTED_NUMBER:
        requested_number = REQUESTED_NUMBER

    if from_number < 0:
        from_number = 0

    if to_number <= 0:
        to_number = from_number + 10

    if from_number > to_number:
        from_number = to_number - 10
    
    return requested_number, from_number, to_number

# Create your views here.
def index(request):
    recipe = RecipeRequest()
    response = HttpResponse()

    query = request.GET.copy()
    print(query)
    '''processing query string'''
    try:
        ingredients = request.GET.get('ingredients').strip().split(',')
        ingredients = list(set(ingredients))
    except AttributeError as ae:
        print(str(ae))
        raise Http404("Query string missing")

    print(ingredients)
    """handle exceptions for numbers"""
    requested_number, from_number, to_number = handle_request_numbers(request=request)
    
    category = request.GET.get('category')
    if category == None:
        category = 'All'
    
    start = timer()
    print('Category:', category)
    print(requested_number, from_number, to_number)

    response_data = recipe.get_request_data(
        ingredients=ingredients, 
        category=category, 
        requested_number=requested_number,\
        from_number=from_number, 
        to_number=to_number)

    stop = timer()

    print(stop - start)

    if request.method == 'GET':
        if response_data:
            '''In order to allow non-dict objects to be serialized set the
                safe parameter to False.'''
            return JsonResponse(response_data, safe=False)
        else:
            return JsonResponse('No data', safe=False)

    
    

def serve_ingredients(request):
    try:
        ingredients = Ingredient.objects.all().values()
        ingredient_list = []
        for idx, ingredient in enumerate(ingredients):
            ingredient_list.append({'id': idx, 'value': ingredient['ingredient'].lower()})

    except:
       raise Http404("Problems while fetching ingredients")

    if request.method == 'GET':
        try:
            response = JsonResponse(ingredient_list, safe=False)
        except TypeError:
            raise Http404("Not a JSON serializable object")
            
        return response