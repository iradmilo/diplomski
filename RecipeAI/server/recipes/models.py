from django.db import models

# Create your models here.
# For class attributes refer to documentation

class Recipe(models.Model):
    recipe = models.CharField(max_length=255, unique=True, blank=False, null=False)
    category = models.ForeignKey('Category', on_delete=models.PROTECT, blank=False, null=False)
    description = models.TextField(blank=True, null=True)
    url = models.URLField(unique=True, blank=False, null=False)
    rating = models.FloatField(blank=False, null=False)
    adjusted_rating = models.FloatField(blank=True, null=True)
    score = models.FloatField(blank=True, null=True)
    author = models.CharField(max_length=300, blank=False, null=False)
    reviews = models.IntegerField(blank=False, null=False)
    prep_time = models.CharField(max_length=20, default=None, blank=True, null=True)
    cook_time = models.CharField(max_length=20, default=None, blank=True, null=True)
    ready_time = models.CharField(max_length=20, default=None, blank=True, null=True)
    calories = models.PositiveSmallIntegerField(default=None, blank=True, null=True)
    servings = models.PositiveSmallIntegerField(blank=False, null=False)
    nr = models.FloatField(blank=True, null=True)
    lim3 = models.FloatField(blank=True, null=True)
    nrf = models.FloatField(blank=True, null=True)
    carb_per = models.PositiveSmallIntegerField(blank=True, null=True)
    fat_per = models.PositiveSmallIntegerField(blank=True, null=True)
    protein_per = models.PositiveSmallIntegerField(blank=True, null=True)
    total_per = models.PositiveSmallIntegerField(blank=True, null=True)
    instruction_steps_total = models.PositiveSmallIntegerField(default=0, blank=True, null=True)
    ingredient_list = models.ManyToManyField(
        'IngredientContent',
        through = 'IngredientList'
    )

    class Meta:
        #table_name
        db_table = 'recipe'
        #setting indexes for faster data manipulation
        indexes = [
            models.Index(fields=['recipe']),
            models.Index(fields=['nrf'])
        ]
    def __str__(self):
        return self.recipe


class Category(models.Model):
    category = models.CharField(max_length=50, unique=True, blank=False, null=False)

    class Meta:
        db_table = 'category'
        models.Index(fields=['category']),
    def __str__(self):
        return self.category


class Nutrient(models.Model):
    nutrient = models.CharField(max_length=30, unique=True, blank=False, null=False)

    class Meta:
        db_table = 'nutrient'
    def __str__(self):
        return self.nutrient


class RecipeNutrient(models.Model):
    recipe = models.ForeignKey('Recipe', on_delete=models.CASCADE, blank=False, null=False)
    nutrient = models.ForeignKey('Nutrient', on_delete=models.CASCADE, blank=False, null=False)
    nutrient_value = models.CharField(max_length=20, blank=False, null=False)
    daily_value = models.PositiveSmallIntegerField(blank=True, null=True)
    
    class Meta:
        db_table = 'recipe_nutrient'
    def __str__(self):
        return str(self.recipe) + ',' + str(self.nutrient)


class Instruction(models.Model):
    instruction = models.TextField(blank=False, null=False)

    class Meta:
        db_table = 'instruction'
        models.Index(fields=['instruction']), 
    def __str__(self):
        return self.instruction


class RecipeInstruction(models.Model):
    recipe = models.ForeignKey('Recipe', on_delete=models.CASCADE, blank=False, null=False)
    instruction = models.ForeignKey('Instruction', on_delete=models.CASCADE, blank=False, null=False)
    step = models.PositiveSmallIntegerField(blank=False, null=False)

    class Meta:
        db_table = 'recipe_instruction'
    def __str__(self):
        return str(self.recipe) + ',' + str(self.instruction)


class IngredientList(models.Model):
    recipe = models.ForeignKey('Recipe', on_delete=models.CASCADE, blank=False, null=False)
    ingredient_content = models.ForeignKey('IngredientContent',\
        on_delete=models.CASCADE, blank=False, null=False)

    class Meta:
        db_table = 'ingredient_list'
    def __str__(self):
            return str(self.recipe) + ',' +\
            str(self.ingredient_content)


class IngredientContent(models.Model):
    content = models.CharField(max_length=255, unique = True, blank=False, null=False)

    class Meta:
        db_table = 'ingredient_content'
    def __str__(self):
        return self.content


class RecipeIngredient(models.Model):
    recipe = models.ForeignKey('Recipe', on_delete=models.CASCADE, blank=False, null=False)
    ingredient = models.ForeignKey('Ingredient',\
        on_delete=models.CASCADE, blank=False, null=False)

    class Meta:
        db_table = 'recipe_ingredient'
    def __str__(self):
            return str(self.recipe) + ',' +\
            str(self.ingredient)


class Ingredient(models.Model):
    ingredient = models.CharField(max_length=255, unique=True, blank=False, null=False)
    category_subcategory = models.ForeignKey('CategorySubcategory', on_delete=models.PROTECT, blank=True, null=True)
    ingredient_type = models.ForeignKey('IngredientType', on_delete=models.PROTECT, blank=True, null=True)
    recipes = models.ManyToManyField(
        'Recipe', 
        through = 'RecipeIngredient',
        )

    class Meta:
        db_table = 'ingredient'
        models.Index(fields=['ingredient'])
    def __str__(self):
        return self.ingredient


class IngredientType(models.Model):
    type_name = models.CharField(max_length=20, unique=True, blank=False, null=False)

    class Meta:
        db_table = 'ingredient_type'
        models.Index(fields=['type_name']),
    def __str__(self):
        return self.type_name


class IngredientCategory(models.Model):
    category = models.CharField(max_length=100, unique=True, blank=False, null=False)

    class Meta:
        db_table = 'ingredient_category'
        models.Index(fields=['category']),
        
    def __str__(self):
        return self.category


class IngredientSubcategory(models.Model):
    subcategory = models.CharField(max_length=100, unique=True, blank=False, null=False)

    class Meta:
        db_table = 'ingredient_subcategory'
        models.Index(fields=['subcategory']),
    def __str__(self):
        return self.subcategory


class CategorySubcategory(models.Model):
    ingredient_category = models.ForeignKey('IngredientCategory', on_delete=models.CASCADE, blank=False, null=False)
    ingredient_subcategory = models.ForeignKey('IngredientSubcategory', on_delete=models.CASCADE, blank=False, null=False)

    class Meta:
        db_table = 'category_subcategory'
    def __str__(self):
        return str(self.ingredient_category) + ',' + str(self.ingredient_subcategory)


class Compound(models.Model):
    compound = models.CharField(max_length=100, unique=True, blank=False, null=False)
    effect = models.CharField(max_length=100, blank=False, null=False)

    class Meta:
        db_table = 'compound'

        indexes = [
            models.Index(fields=['compound']),
            models.Index(fields=['effect'])
        ]
    def __str__(self):
        return self.compound


class IngredientCompound(models.Model):
    ingredient = models.ForeignKey('Ingredient', on_delete=models.CASCADE, blank=False, null=False)
    compound = models.ForeignKey('Compound', on_delete=models.CASCADE, blank=False, null=False)

    class Meta:
        db_table = 'ingredient_compound'
    def __str__(self):
        return str(self.ingredient) + ',' + str(self.compound)