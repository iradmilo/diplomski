'''Script for populating recipe categories'''
import os
import sys
import json
from pprint import pprint
from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from ...models import Category
from os import listdir
from os.path import isfile, join

''' file_path '''
module_dir = os.path.dirname(__file__)
PROJECT_ROOT = os.path.realpath(os.path.dirname(module_dir))
DIR_PATH = os.path.join(PROJECT_ROOT, 'commands/DBManagement/data')
JSONFILES_FOLDER = [os.path.join(PROJECT_ROOT, 'commands/DBManagement/data/' + f) for f in listdir(DIR_PATH) if isfile(join(DIR_PATH, f))]


class Command(BaseCommand):

    def add_category(self, category):
        try:
            category = Category.objects.get_or_create(category=category)
            return category
        except:
            print("Something went wrong while populating categories")

    def populate(self, recipe):
        self.add_category(category=recipe)
        """ Print out what was added """
        for category in Category.objects.all():
            print("The following category been added to the database:" + category)
    
    help = 'Populating recipe category database'

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS(JSONFILES_FOLDER))

        self.stdout.write(self.style.SUCCESS('Successfully called script populate recipe categories'))

        for file_path in JSONFILES_FOLDER:
            self.stdout.write(self.style.WARNING(file_path))
            with open (file_path) as f:
                data = json.load(f)

            try:
                self.stdout.write(self.style.SUCCESS(data[0]['category']))
                self.populate(data[0]['category'])      
            except:
                continue
        
        
        

