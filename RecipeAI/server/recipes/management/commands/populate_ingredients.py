'''
Script for populating nutrients
populating subcategories, categories, and ingredients, then ingredient list and content
!precondition recipes
'''
import os
import sys
import json
import re
from django.db import IntegrityError
import pandas as pd
from django.core.management.base import BaseCommand, CommandError
from ...models import Recipe, Ingredient, Compound, IngredientCompound, IngredientCategory,\
 IngredientSubcategory, CategorySubcategory, IngredientType, IngredientContent, IngredientList,\
 RecipeIngredient
from os import listdir
from os.path import isfile, join
from numba import jit, njit
from timeit import default_timer as timer

# file_path
FOODS='foods.csv'
module_dir = os.path.dirname(__file__)
PROJECT_ROOT = os.path.realpath(os.path.dirname(module_dir))
DIR_PATH = os.path.join(PROJECT_ROOT, 'commands/DBManagement/data')
JSONFILES_FOLDER = [os.path.join(PROJECT_ROOT, 'commands/DBManagement/data/' + f)\
    for f in listdir(DIR_PATH) if isfile(join(DIR_PATH, f))]
foods_file_path = os.path.join(PROJECT_ROOT, 'commands/DBManagement/' + FOODS)

class Command(BaseCommand):

    '''
    Helper Function for updating table for wanted ingredient 
    @param names Names of ingredients you want to update in table
    '''
    def populate_specific_ingredient(self, names):
        recipes = Recipe.objects.all()
        ingredients = []
        for name in names:
            ingredients.append(Ingredient.objects.get(ingredient=name))
        print(ingredients)
        start = timer()
        for counter, recipe in enumerate(recipes):
            ingredient_content = IngredientList.objects.filter(recipe=recipe.pk)
            ingredient_list = []
            for ingredient in ingredient_content:
                ingredient_list.append(str(ingredient.ingredient_content))
            ingredient_string = ''.join(ingredient_list)
            
            for ingredient in ingredients:
                if self.findIngredient(ingredient.ingredient, ingredient_string):
                    try:
                        RecipeIngredient.objects.get_or_create(recipe=recipe, ingredient=ingredient)
                    except:
                        pass
            print(counter)
        stop = timer()
        print("Total time", stop-start)

    '''
    regular expression search for ingredient in ingredient text
    @param ingredient to search for
    @param sentence ingredient text
    @return 
    '''
    @jit
    def findIngredient(self, ingredient, sentence):
        return re.search(r'\b({0})(s|es)?\b'.format(ingredient), sentence, flags=re.IGNORECASE)
            
    '''
    populating ingredient list connection between ingredient, ingredient content and recipe
    @param data whole set of recipes
    '''
    def populate_recipe_ingredient(self):
        recipes = Recipe.objects.all()
        ingredients = Ingredient.objects.all()
        start = timer()
        for counter, recipe in enumerate(recipes):
            ingredient_content = IngredientList.objects.filter(recipe=recipe.pk)
            ingredient_list = []
            for ingredient in ingredient_content:
                ingredient_list.append(str(ingredient.ingredient_content))
            ingredient_string = ' '.join(ingredient_list)
            print(ingredient_string)
            for ingredient in ingredients:
                if self.findIngredient(ingredient.ingredient, ingredient_string):
                    try:
                        RecipeIngredient.objects.get_or_create(recipe=recipe, ingredient=ingredient)
                    except:
                        pass
            print(counter)
        stop = timer()
        print("Total time", stop-start)

    """
    populating ingredient list table
    @param data
    """
    def populate_ingredient_list(self, data):
        i = 0
        for recipe in data:
            j = 0
            for ingredient_content in recipe['ingredients']:
                print(i, ' , ', j)
                try:
                    IngredientContent.objects.get_or_create(content=ingredient_content)
                except:
                    print("Error while inserting ingredient content")
                try:
                    IngredientList.objects.get_or_create(\
                            recipe=Recipe.objects.get(recipe=recipe['name']),\
                            ingredient_content=IngredientContent.objects.get(content=ingredient_content))
                except:
                    print("Error while inserting ingredient content") 

                j += 1
            i += 1

    """Populating ingredient types, categories, subcategories, cat_subs, and
    ingredients"""
    def populate_ingredient(self, foods):
        names = foods.name
        categories = foods.food_group
        subcategories = foods.food_subgroup
        types = foods.food_type
        for counter, name in enumerate(names):
            try:
                
                t = IngredientType.objects.get_or_create(type_name=types[counter])
                print(t)
                category = IngredientCategory.objects.get_or_create(category=categories[counter])
                print(category)
                subcategory = IngredientSubcategory.objects.get_or_create(subcategory=subcategories[counter])
                print(subcategory)
                category_sub = CategorySubcategory.objects.get_or_create(\
                                    ingredient_category=IngredientCategory.objects.get(\
                                        category=categories[counter]),\
                                    ingredient_subcategory=IngredientSubcategory.objects.get(\
                                        subcategory=subcategories[counter])
                                )
                print(category_sub)                                                         
                ingredient = Ingredient.objects.get_or_create(\
                                    ingredient=name,\
                                    category_subcategory=CategorySubcategory.objects.get(\
                                        ingredient_category=IngredientCategory.objects.get(\
                                            category=categories[counter]),\
                                        ingredient_subcategory=IngredientSubcategory.objects.get(\
                                            subcategory=subcategories[counter])
                                    ),
                                    ingredient_type=IngredientType.objects.get(type_name=types[counter])
                                )    
                print(ingredient)
            except IntegrityError as ie:
                print("Error occurred while inserting ingredient" + str(ie))


    help = 'Populating ingredient database'

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS(JSONFILES_FOLDER))

        self.stdout.write(self.style.SUCCESS('Successfully called script populate ingredients'))

        """
        foods.csv file
        loads file with pandas
        """
        food_data = pd.read_csv(foods_file_path, encoding='latin')

        """populating ingredients only from foods.cvs file"""
        #self.populate_ingredient(food_data)
        
        """populating ingredient list for all recipe categories"""
        for file_path in JSONFILES_FOLDER:
            self.stdout.write(self.style.WARNING(file_path))
            with open (file_path) as f:
                data = json.load(f)
            #self.populate_ingredient_list(data)
        #self.populate_recipe_ingredient()
        self.populate_specific_ingredient(['Milk'])
