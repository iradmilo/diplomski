import os
import sys
import re
from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from timeit import default_timer as timer
from ...models import Recipe, Ingredient, RecipeNutrient, RecipeInstruction, IngredientList
from django.db.models import prefetch_related_objects
import json
from pprint import pprint
from numba import jit

class Command(BaseCommand):

    def regular_expression(self, ingredient, ingredient_string):
        return re.search(r'\b({0})(e|es)?\b'.format(ingredient), ingredient_string, flags=re.IGNORECASE)

    def findIngredients(self, ingredients, ingredient_list):
        present_ingredients = []
        ingredient_string = ''.join(ingredient_list)
        for ingredient in ingredients:
            if self.regular_expression(ingredient=ingredient, ingredient_string=ingredient_string):
                present_ingredients.append(ingredient)
        return present_ingredients

    # pk__in
    # by category ???
    help = 'Populating ingredient database'
    def handle(self, *args, **options):
        ingredients = ['Butter', 'Honey', 'Eggs', 'Almond', 'Beef', 'Beer', 'Onion', 'Garlic'\
        'Peanut', 'Pineapple', 'Lemon', 'Coffee', 'Cucumber', 'Fig', 'Salt', 'Paprika']  
        responseObject = []
        final_recipe_instructions = []
        final_recipe_nutrients = []
        recipeArray = []
        dictRecipes = {}
        ingr_list_length = len(ingredients)

        '''timer start'''
        start = timer()

        '''Mysql can use only 61 tables in join'''
        if ingr_list_length >= 60:
            ingr_list_length = 60

        try:
            inter_list = []
            start1 = timer()
            try:
                recipes = Ingredient.objects.get(ingredient=ingredients[0]).recipes.all()
            except:
                pass
            inter_list.append(recipes)
            for i in range(1, ingr_list_length):
                try:
                    recipesAdd = Ingredient.objects.get(ingredient=ingredients[i]).recipes.all()
                except:
                    pass
                inter_list.append(recipesAdd)
                recipes |= recipesAdd

            stop1 = timer()
            print("Time recipes union", stop1 - start1)

            # '''intersection part'''
            # start6 = timer()
            # print(inter_list)
            # list_len = len(inter_list)
            # while list_len > 0:
            #     all_elem = inter_list[0]
            #     for counter in range(1, list_len) 
            #         all_elem & = inter_list[counter]
            #     if not all_elem:

                
            
            # stop6 = timer()
            # print("Inter part", stop6 - start6)

            start5 = timer()
            '''dict for asserting number of times required ingredients show in specific recipe'''
            for recipe in recipes:
                try:
                    dictRecipes[recipe.pk] += 1
                except KeyError: 
                    dictRecipes.update({recipe.pk: 1})

            stop5 = timer()
            print("Dict stuff", stop5 - start5)

            start2 = timer()
            '''in sorted fun key is key by which to sort in our case its dict value
            hence 1; reverse = True for descending order we want those recipes
            that appear greater number of times'''
            recipes = Recipe.objects.filter(pk__in=[k for k, v in sorted(dictRecipes.items(),\
                                                    key=lambda x:x[1], reverse=True)[:10]])
            # print("Recipes before sorting: \n", recipes)

            '''reverse == descending order'''
            recipes = recipes.order_by('score', 'nrf').reverse()
            #print("Recipes after sorting: \n", recipes)
            stop2 = timer()
            print("Sorting recipes", stop2 - start2)

            start3 = timer()
            recipe_nutrients = [RecipeNutrient.objects.filter(recipe=recipe) for recipe in recipes]

            recipe_instructions = [[instructions\
                for instructions in RecipeInstruction.objects.filter(recipe=recipe)]\
                for recipe in recipes]

            for instructions in recipe_instructions:
                instr_Object = {}
                for instruction in instructions:
                    instr_Object.update({instruction.step: instruction.instruction.instruction})
                final_recipe_instructions.append(instr_Object)

            stop3 = timer()
            print("Additional stuff to core recipe", stop3 - start3)
            
            start4 = timer()
            for nutrient_list in recipe_nutrients:
                for nutrients in nutrient_list:
                    recipe_nutrients = []      
                    for nutrient in nutrient_list:
                        recipe_nutrients.append(
                            {   
                                'values': {
                                    'daily_value':nutrient.daily_value,
                                    'value':nutrient.nutrient_value
                                },
                                'nutrient': nutrient.nutrient.nutrient
                            }
                        )        
                    final_recipe_nutrients.append(recipe_nutrients)

            
            
            
            for counter, recipe in enumerate(recipes):
                ingredient_list = [content.content for content in recipe.ingredient_list.all()]
                ingredients_present = self.findIngredients(ingredients, ingredient_list)
                #print(ingredients_present)
                responseObject.append({
                    'ingredient_list':ingredient_list,
                    'instructions':final_recipe_instructions[counter],
                    'nutrients':final_recipe_nutrients[counter],
                    'total_steps': recipe.instruction_steps_total,
                    'recipe':recipe.recipe,
                    'category':recipe.category.category,
                    'nr':recipe.nr,
                    'lim3':recipe.lim3,
                    'nrf':recipe.nrf,
                    'url':recipe.url,
                    'score':recipe.score,
                    'prep_time':recipe.prep_time,
                    'cook_time':recipe.cook_time,
                    'ready_time':recipe.ready_time,
                    'calories':recipe.calories,
                    'servings':recipe.servings,
                    'carb_per':recipe.carb_per,
                    'fat_per':recipe.fat_per,
                    'protein_per':recipe.protein_per,
                    'total_per':recipe.total_per,
                    'ingredients_in_recipe':ingredients_present
                })
            
            stop4 = timer()
            print("JSON and parsing part", stop4 - start4)
            
            pprint(responseObject[0])
            # dictionaryToJson = json.dumps(responseObject)
            # pprint(dictionaryToJson[0])
            # responseObject = json.loads(dictionaryToJson)
            # pprint(responseObject[0])

        except Ingredient.DoesNotExist as dne:
            print(str(dne))

        '''timer stop'''
        end = timer()

        print("Total time: ", end - start)