'''
Script for populating recipe total number of steps(instruction_steps_total property)
!precondition everything
'''
import os
import sys
from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from ...models import Recipe, RecipeInstruction
from timeit import default_timer as timer

class Command(BaseCommand):

    help = 'Populating recipe database'

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Successfully called script populate recipe steps'))

        start = timer()
        
        recipes = Recipe.objects.all()
        for recipe in recipes:
            last_recipe_instruction = RecipeInstruction.objects.filter(recipe=recipe).last()
            Recipe.objects.filter(recipe=recipe.recipe).update(instruction_steps_total=last_recipe_instruction.step)

        end = timer()
        print("Calculation last step time: ", end - start)