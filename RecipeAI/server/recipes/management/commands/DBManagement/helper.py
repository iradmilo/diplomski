import os
import json
from timeit import default_timer as timer
from pprint import pprint
from statistics import median
from math import floor, ceil
import pandas as pd
import re
import operator
from math import floor

module_dir = os.path.dirname(__file__)
PROJECT_ROOT = os.path.realpath(os.path.dirname(module_dir))

#pretty print data
def pretty_print(data):
	for rec in data:	
		pprint(rec)
		
#check if some fat, carbs, protein attributes are measured in mg 
def test_mg(fat, carbs, protein):
	try:
		(f, c, p) = (fat.find('mg'), carbs.find('mg'), protein.find('mg'))
	except:
		print("not ideal")
	
	if (f, c, p) != (-1, -1, -1):
		print(a, b, c)

# def convert_value(value):
# 	num = re.findall(r'[-+]?\d+\.\d+|\d+', value)
# 	print('Num: ', num[0])
# 	unit = value.sub(value, num[0])
# 	print('Unit: ', unit)
# 	num = int(num or 0)
	
# 	if unit == 'g':
# 		return num
# 	elif unit == 'mg':
# 		return num / 1000
# 	elif unit == 'dg':
# 		return num / 10
# 	elif unit == 'cg':
# 		return num / 100
# 	elif unit == 'kg':
# 		return num * 1000
# 	elif unit == 'dkg':
# 		return num * 10
# 	else:
#		print("Something else")

# @param value - value of ingredient 
# @return number from nutrient text
def parse_value(value):
	return re.findall(r'[-+]?\d+\.\d+|\d+', value)

adjust_nutrient = lambda cal_per_serv: [100 / cal_per_serv * 100 if cal_per_serv != 0 else 0][0]

# def calculate_value(value, scal, num):
# 	return float(parse_value(value)[0]) * adjust_nutrient(scal) / num

# @param value - value of ingredient in standardized unit for this calculation
# @param num - value to divide with RDV for NutrientRich 
# and MDV for Nutrients to be limites 
# @return NR score
def calculate_food_richness(value, num):
 	return float(parse_value(value)[0]) / num

# @param value - value of ingredient in standardized unit for this calculation
# @param cal - calories per type of food (fat, carbs, protein)
# @param scal - calorie servings
# @return nutrient percentage in recipe
def calculate_per(value, cal, scal):
	return floor(floor(float(parse_value(value)[0])) * cal / scal * 100)

# macronutrient balance and nutricionist critera for scoring recipes
# Nutrient Rich(nr) - Protein, Fiber, Calcium, Potassium, Magnesium, Iron
# Vitamin A, Vitamin C, Vitamin B1, Vitamin B3, Vitamin B6, Folate
# Nutrients to Limit - Saturated Fats, Sugars, Sodium
def nutrients(data):
	for recipe in data:
		scal = int(recipe['calories'] or 0)
		print("calories_per_serving: ", scal)
		fat_per = 0; carbs_per = 0; protein_per = 0; total_per = 0; 
		lim3 = 0; nr = 0; nrf = 0
		sugars = 0; sodium = 0; sat_fat = 0
		protein = 0; fiber = 0; calcium = 0; 
		potassium = 0; magnesium = 0; iron = 0; 
		vitaminA = 0; vitaminC = 0; thiamin = 0; 
		niacin = 0; vitaminB6 = 0; folate = 0
		try:
			nutrients = recipe['nutrients']
			for nutrient in nutrients:
				name = nutrient['nutrient_name']
				value = nutrient['nutrient_value']
				if name == 'Total Fat: ':
					fat_per = calculate_per(value=value, cal=9, scal=scal)
				elif name == 'Total Carbohydrates: ':
					carbs_per = calculate_per(value=value, cal=4, scal=scal)
				#Encouraged nutrients 12 total here - divided by RDV
				elif name == 'Protein: ':
					#Protein
					protein_per = calculate_per(value=value, cal=4, scal=scal)
					protein = calculate_food_richness(value, 57)
				elif name == 'Dietary Fiber: ':
					#Fiber
					fiber = calculate_food_richness(value, 25)
				elif name == 'Calcium: ':
					#Calcium
					calcium = calculate_food_richness(value, 1000)
				elif name == 'Potassium: ':
					#Potassium
					potassium = calculate_food_richness(value, 3500)
				elif name == 'Magnesium: ':
					#Magnesium
					magnesium = calculate_food_richness(value, 3500)
				elif name == 'Iron: ':
					#Iron
					iron = calculate_food_richness(value, 3500)
				elif name == 'Vitamin A: ':
					#Vitamin A
					vitaminA = calculate_food_richness(value, 5000)
				elif name == 'Vitamin C: ':
					#Vitamin C
					vitaminC = calculate_food_richness(value, 60)
				elif name == 'Thiamin: ':
					#Vitamin B1
					thiamin = calculate_food_richness(value, 1.1)
				elif name == 'Niacin: ':
					#Vitamin B3
					niacin = calculate_food_richness(value, 16)
				elif name == 'Vitamin B6: ':
					#Vitamin B6
					vitaminB6 = calculate_food_richness(value, 1.4)
				elif name == 'Folate: ':
					#Folate
					folate = calculate_food_richness(value, 200)
				#Limited nutrients - divided by MDV	
				elif name == 'Saturated Fat: ':
					#Saturated Fat
					sat_fat = calculate_food_richness(value, 20)
				elif name == 'Sugars: ':
				 	#Sugars
					sugars = calculate_food_richness(value, 125)
				elif name == 'Sodium: ':
					#Sodium
					sodium = calculate_food_richness(value, 2400)

			nr = protein + fiber + calcium + potassium + magnesium +\
				 iron + vitaminA + vitaminC + thiamin + niacin +\
				 vitaminB6 + folate
			lim3 = sat_fat + sodium + sugars
			total_per = fat_per + carbs_per + protein_per
			nrf = nr - lim3
		except:
			print("can't get data")
	
		print("Recipe: %s\
			   \nfat_per = %s,\
			   \ncarbs_per = %s,\
			   \nprotein_per = %s,\
			   \ntotal_per = %s,\
			   \nlim3 = %s,\
			   \nnr = %s,\
			   \nnrf = %s,\
			   \nend\n"\
			   %(recipe['name'],\
			   fat_per,\
			   carbs_per,\
			   protein_per,\
			   total_per,\
			   lim3,\
			   nr,\
			   nrf\
			  ))
		# if nrf < 0:
		# 		print("Not good")
		# 		break

#get median
def get_median(data):
	score_list = []
	for recipe in data:
		score_list.append(recipe['num_reviews'])
	return median(score_list)
	
#ponder score with numer of reviews
#if num_reviews is 0 than final score is zero
def adjust_score(data):
	print("Total number of recepies in %s category: %s\n" %(data[0]['category'], len(data)))
	recipe_no = 1
	median = get_median(data)
	for recipe in data:
		old_rating = recipe['rating']
		new_rating = old_rating
		num_reviews = recipe['num_reviews']
		score = new_rating / 5.0
		if num_reviews < median:
			new_rating = (old_rating * num_reviews) / median
			score = new_rating / 5.0
		
		print("Recipe name: %s\
			  \nNumber: %s\
			  \nNum reviews: %s\
			  \nMedian: %s\
			  \nOld rating: %s\
			  \nAdjusted rating: %s\
			  \nFinal score: %s\n\n"\
			   %(recipe['name'],\
			   recipe_no,\
			   num_reviews,\
			   median, old_rating,\
			   new_rating,\
			   score))

		recipe_no += 1

#function for parsing and arranging ingredients
def parse_ingredients():
	
	path_ingredients = os.path.join(PROJECT_ROOT, 'ingredients.json')
	
	list = []
	
	with open(path_ingredients) as file:
		ingredients = json.load(file)
	
	
	path_foods = os.path.join(PROJECT_ROOT, 'foods.csv')
	
	for ingredient in ingredients[0]['ingredients']:
		string = ingredient['ingredient'].lower()
		list.append(re.sub(r'\(.*\)', '', string))
	
	df = pd.read_csv(path_foods, encoding='latin')
	name_list = df.name
	
	for ingredient in name_list:
		string = ingredient.lower()
		if ingredient.lower() not in list:	
			list.append(re.sub(r'\(.*\)', '', string))
	
	print(list)
	print(len(list))

# pseudo code for calculating probabilities
# def calculate_probability(recipes, ingredients):
# 	dictAll = {}
# 	dictB = {}
# 	l = len(ingredients)
# 	for ingredient in ingredients:
# 		results = search in base all recipes with ingredient
# 			for res in results:
# 				try:
# 					dictB[reipeId] +=1
# 				except:
# 					dictB.update({recipeId : 1})
# 	for recipe in recipes:
# 		dictAll.update{recipe[id]: recipe[score]}
# 		if recipe.id in dictB:
# 			dictAll[value] = recipe[score] * pow(0.1, l - dictB[recipe.id].value)
# 		else
# 			dictAll[value] = ecipe[score] * pow(0.1, l)
# 	# for key, value in sorted(dictAll.iteritems(), key=lambda (k,v): (v,k)):
#     # 	print "%s: %s" % (key, value)
# 	sorted_x = sorted(x.items(), key=operator.itemgetter(1))

#Function for parsing category and subcategory names
#in foods.csv file
def get_ingredient_categories_and_sub():
	path_foods = os.path.join(PROJECT_ROOT, 'foods.csv')

	category = []; subcategory = []
	df = pd.read_csv(path_foods, encoding='latin')

	names = []
	categories = []
	subcategories = []
	types = []

	name_list = df.name
	category_list = df.food_group
	subcategory_list = df.food_subgroup
	type_list = df.food_type
	
	[names.append(re.sub(r'\(.*\)', '', ingredient.lower()))\
	 for ingredient in name_list\
	 if ingredient.lower() not in names]
	
	[categories.append(category) for category in category_list\
	 if category not in categories]

	[subcategories.append(subcategory) for subcategory in subcategory_list\
	 if subcategory not in subcategories]

	[types.append(t) for t in type_list if t not in types]

	print(names)
	print(categories)
	print(subcategories)
	print(types)
	print(len(names))

	print(name_list[0])
	print(category_list[0])
	print(subcategory_list[0])
	print(type_list[0])

#main start	
if __name__ == "__main__":
	
	path = os.path.join(PROJECT_ROOT, 'data/main-dish.json')
	
	print(path)
	with open(path) as f:
		start = timer()
		data = json.load(f)
		end = timer()
	
	print("time: %s" %(end - start))
	
	get_ingredient_categories_and_sub()
	#pretty_print(data)
	#nutrients(data)
	#parse_ingredients()
	#adjust_score(data)
	#pretty_print(data)
	#nutrients(data)