'''
Script for populating nutrient names and nutrition table
! precondition recipes
'''
import os
import sys
import json
from os import listdir
from django.db import IntegrityError
import re
from os.path import isfile, join
from pprint import pprint
from django.core.management.base import BaseCommand, CommandError
from ...models import Nutrient, Recipe, RecipeNutrient

""" file_path """
module_dir = os.path.dirname(__file__)
PROJECT_ROOT = os.path.realpath(os.path.dirname(module_dir))
DIR_PATH = os.path.join(PROJECT_ROOT, 'commands/DBManagement/data')
JSONFILES_FOLDER = [os.path.join(PROJECT_ROOT, 'commands/DBManagement/data/' + f)\
 for f in listdir(DIR_PATH) if isfile(join(DIR_PATH, f))]


class Command(BaseCommand):

    def return_name(self, nutrient):
        return nutrient['nutrient_name'].rsplit(': ')[0]

    '''
    @param recipe name
    @param nutrients list
    !important some recipes are duplicated through categories so
    there will be 86*18 Something went wrong messages
    for set data
    '''
    def connect_recipe_and_nutrient(self, recipe, nutrients):
        if nutrients:
            for nutrient in nutrients:
                nutrient_name = self.return_name(nutrient)
                value = nutrient['nutrient_value']
                daily_value = nutrient['daily_value']
                """if daily value is different from None then extract first number"""
                if daily_value:
                    daily_value = re.findall(r'\d+', daily_value)[0]

                try:
                    recipe_nutrient = RecipeNutrient.objects.get_or_create(\
                        recipe = Recipe.objects.get(recipe=recipe),\
                        nutrient = Nutrient.objects.get(nutrient=nutrient_name),\
                        nutrient_value=value,\
                        daily_value=daily_value\
                    )
                except IntegrityError as ie:
                    print("Something went wrong while adding recipe_nutrient table" + str(ie))
        else:
            print("Nutrients list is empty")

    '''
    @param nutrient
    @return nutrient 
    '''
    def add_nutrient(self, nutrient):
        nutrient_name = self.return_name(nutrient)
        nutrient = Nutrient.objects.get_or_create(nutrient=nutrient_name)
        return nutrient

    '''
    @param recipe name
    @param nutrients list
    '''
    def populate(self, nutrients):
        for nutrient in nutrients:
            self.add_nutrient(nutrient=nutrient)
    
    help = 'Populating nutrient database'

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Successfully called script populate nutrients'))

        '''inserting only nutrient names'''
        with open (JSONFILES_FOLDER[0]) as fi:
                data = json.load(fi)
        try:
            self.populate(data[0]['nutrients'])  
        except:
            print("Nutrient names failed to insert")
        
        '''inserting nutrient values for all recipes'''
        for file_path in JSONFILES_FOLDER:
            self.stdout.write(self.style.WARNING(file_path))
            with open (file_path) as f:
                data = json.load(f)

            for recipe in data:
                self.connect_recipe_and_nutrient(recipe['name'], recipe['nutrients'])
            