'''
Script for populating recipe table
! precondition category
'''
import os
import sys
import json
import re
from django.db import IntegrityError
from statistics import median
from pprint import pprint
from django.core.management.base import BaseCommand, CommandError
from ...models import Recipe, Category
from os import listdir
from math import floor, ceil
from os.path import isfile, join

module_dir = os.path.dirname(__file__)
PROJECT_ROOT = os.path.realpath(os.path.dirname(module_dir))
DIR_PATH = os.path.join(PROJECT_ROOT, 'commands/DBManagement/data')
JSONFILES_FOLDER = [os.path.join(PROJECT_ROOT, 'commands/DBManagement/data/' + f)\
    for f in listdir(DIR_PATH) if isfile(join(DIR_PATH, f))]

class Command(BaseCommand):

    '''
    re library needs to be included
    @param value of ingredient 
    @return number from nutrient text
    '''
    def parse_value(self, value):
        return re.findall(r'[-+]?\d+\.\d+|\d+', value)

    '''
    next 2 functions not used
    but could be necessary in future
    for adjusting lim3,nr,nrf to 100 cal
    '''
    adjust_nutrient = lambda cal_per_serv: [100 / cal_per_serv * 100 if cal_per_serv != 0 else 0][0]

    def calculate_value(value, scal, num):
        return float(self.parse_value(value)[0]) * adjust_nutrient(scal) / num
    
    '''
    @param value of ingredient in standardized unit for this calculation
    @param num value to divide with RDV for NutrientRich 
    and MDV for Nutrients to be limited
    @return NR score
    '''
    def calculate_food_richness(self, value, num):
        return float(self.parse_value(value)[0]) / num

    '''
    @param value value of ingredient in standardized unit for this calculation
    @param cal calories per type of food (fat, carbs, protein)
    @param scal calorie servings
    @return nutrient percentage in recipe
    '''
    def calculate_per(self, value, cal, scal):
        return floor(floor(float(self.parse_value(value)[0])) * cal / scal * 100)

    '''
    macronutrient balance and nutricionist critera for scoring recipes
    Nutrient Rich(nr) - Protein, Fiber, Calcium, Potassium, Magnesium, Iron
    Vitamin A, Vitamin C, Vitamin B1, Vitamin B3, Vitamin B6, Folate
    Nutrients to Limit - Saturated Fats, Sugars, Sodium
    @param recipe
    @return_value list containing nr, lim3, nrf, carb_per, fat_per, protein_per, total_per
    '''
    def process_recipe(self, recipe):   
        scal = int(recipe['calories'] or 1)
        fat_per = 0; carb_per = 0; protein_per = 0; total_per = 0; 
        lim3 = 0; nr = 0; nrf = 0
        sugars = 0; sodium = 0; sat_fat = 0
        protein = 0; fiber = 0; calcium = 0; 
        potassium = 0; magnesium = 0; iron = 0; 
        vitaminA = 0; vitaminC = 0; thiamin = 0; 
        niacin = 0; vitaminB6 = 0; folate = 0
        try:
            nutrients = recipe['nutrients']
            for nutrient in nutrients:
                name = nutrient['nutrient_name']
                value = nutrient['nutrient_value']
                
                if name == 'Total Fat: ':
                    fat_per = self.calculate_per(value=value, cal=9, scal=scal)
                elif name == 'Total Carbohydrates: ':
                    carb_per = self.calculate_per(value=value, cal=4, scal=scal)
                """Encouraged nutrients 12 total here - divided by RDV"""
                elif name == 'Protein: ':
                    #Protein
                    protein_per = self.calculate_per(value=value, cal=4, scal=scal)
                    protein = self.calculate_food_richness(value, 57)
                elif name == 'Dietary Fiber: ':
                    #Fiber
                    fiber = self.calculate_food_richness(value, 25)
                elif name == 'Calcium: ':
                    #Calcium
                    calcium = self.calculate_food_richness(value, 800)
                elif name == 'Potassium: ':
                    #Potassium
                    potassium = self.calculate_food_richness(value, 2000)
                elif name == 'Magnesium: ':
                    #Magnesium
                    magnesium = self.calculate_food_richness(value, 375)
                elif name == 'Iron: ':
                    #Iron
                    iron = self.calculate_food_richness(value, 14)
                elif name == 'Vitamin A: ':
                    #Vitamin A
                    vitaminA = self.calculate_food_richness(value, 800)
                elif name == 'Vitamin C: ':
                    #Vitamin C
                    vitaminC = self.calculate_food_richness(value, 80)
                elif name == 'Thiamin: ':
                    #Vitamin B1
                    thiamin = self.calculate_food_richness(value, 1.1)
                elif name == 'Niacin: ':
                    #Vitamin B3
                    niacin = self.calculate_food_richness(value, 16)
                elif name == 'Vitamin B6: ':
                    #Vitamin B6
                    vitaminB6 = self.calculate_food_richness(value, 1.4)
                elif name == 'Folate: ':
                    #Folate
                    folate = self.calculate_food_richness(value, 200)
                """Limited nutrients - divided by MDV"""	
                elif name == 'Saturated Fat: ':
                    #Saturated Fat
                    sat_fat = self.calculate_food_richness(value, 20)
                elif name == 'Sugars: ':
                    #Sugars
                    sugars = self.calculate_food_richness(value, 90)
                elif name == 'Sodium: ':
                    #Sodium
                    sodium = self.calculate_food_richness(value, 2400)
                elif name == 'Cholesterol: ':
                    pass
                else:
                    print("Unexpected, error occurred")

            nr = protein + fiber + calcium + potassium + magnesium +\
                iron + vitaminA + vitaminC + thiamin + niacin +\
                vitaminB6 + folate
            lim3 = sat_fat + sodium + sugars
            total_per = fat_per + carb_per + protein_per
            nrf = nr - lim3
            return [nr, lim3, nrf, carb_per, fat_per, protein_per, total_per]
        except:
            print("Not able to get data")
            return [nr, lim3, nrf, carb_per, fat_per, protein_per, total_per]

    '''
    @param data from recipes
    @return median of recipe category 
    '''
    def get_median(self, data):
        score_list = []
        for recipe in data:
            score_list.append(recipe['num_reviews'])
        return median(score_list)

    '''
    function for adjusting recipe score by number of reviews
    rating of recipes above median point stays the same
    and rating of recipes below median is adjusted
    finally score is set by dividing new rating by 5.0
    because adjusted score can fall below 1.0
    @param median 
    @param recipe
    @return list containing new_rating and score
    '''
    def adjust_score(self, median, recipe):
        old_rating = recipe['rating']
        new_rating = old_rating
        num_reviews = recipe['num_reviews']
        score = new_rating / 5.0
        if num_reviews < median:
            new_rating = (old_rating * num_reviews) / median
            score = new_rating / 5.0
  
        return [new_rating, score]
            
    '''
    function inserting recipes into database
    @param data of recipes in category 
    !important 
    some recipes won't insert because recipe name
    is unique and 86 recipes from collected data 
    are duplicated in more categories than one
    '''
    def populate_recipes(self, data):
        median = self.get_median(data)
        error_num = 0

        for recipe in data:
            adjusted_rating, score = self.adjust_score(median, recipe)
            nr, lim3, nrf, carb_per, fat_per, protein_per, total_per = self.process_recipe(recipe)

            try:
                prep_time = recipe['prep_time']
            except:
                prep_time = None
            
            try:
                cook_time = recipe['cook_time']
            except:
                cook_time = None
            
            try:
                ready_time = recipe['ready_in_time']
            except:
                ready_time = None

            try:
                calories = recipe['calories']
            except:
                calories = None
            try:               
                recipe = Recipe.objects.get_or_create(recipe=recipe['name'],\
                                                      category=Category.objects.get(\
                                                        category=recipe['category']),\
                                                      description=recipe['description'],\
                                                      url=recipe['url'],\
                                                      rating=recipe['rating'],\
                                                      adjusted_rating=adjusted_rating,\
                                                      score=score,\
                                                      author=recipe['author'],\
                                                      reviews=recipe['num_reviews'],\
                                                      prep_time=prep_time,\
                                                      cook_time=cook_time,\
                                                      ready_time=ready_time,\
                                                      calories=calories,\
                                                      servings=recipe['servings'],\
                                                      nr=nr,\
                                                      lim3=lim3,\
                                                      nrf=nrf,\
                                                      carb_per=carb_per,\
                                                      fat_per=fat_per,\
                                                      protein_per=protein_per,\
                                                      total_per=total_per
                                                     )
            except IntegrityError as ie:
                error_num += 1
                print(ie)
        print(error_num)


    help = 'Populating recipe database'

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Successfully called script populate recipes'))

        for file_path in JSONFILES_FOLDER:
            with open (file_path) as f:
                data = json.load(f)
            
            self.populate_recipes(data)