import os
import sys
import re
from ...models import Recipe, Ingredient, IngredientList, RecipeIngredient
from numba import jit, njit
from timeit import default_timer as timer
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):

    @jit
    def findIngredient(self, ingredient, sentence):
        return re.search(r'\b({0})(s|es)\b'.format(ingredient), sentence, flags=re.IGNORECASE)


    def improve_recipe_ingredient(self):
        recipes = Recipe.objects.all()
        ingredients = Ingredient.objects.all()
        number = 0
        start = timer()
        for counter, recipe in enumerate(recipes):
            ingredient_content = IngredientList.objects.filter(recipe=recipe.pk)
            ingredient_list = []
            for ingredient in ingredient_content:
                ingredient_list.append(str(ingredient.ingredient_content))
            ingredient_string = ' '.join(ingredient_list)
            for ingredient in ingredients:
                if self.findIngredient(ingredient.ingredient, ingredient_string):
                    try:
                        print(recipe)
                        print(ingredient)
                        RecipeIngredient.objects.get_or_create(recipe=recipe, ingredient=ingredient)
                        number += 1
                    except:
                        print("Error")
                        pass
            if counter % 1000 == 0:
                print("Counter:  ", counter)
        print("Total number", number)
        stop = timer()
        print("Total time", stop-start)


    help = 'Populating recipe database'

    def handle(self, *args, **options):
        self.improve_recipe_ingredient()