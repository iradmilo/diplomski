'''
script for changing nrf value
!precondition Recipe, RecipeNutrients, Nutrients
'''
import re
import os
import sys
from django.core.management.base import BaseCommand, CommandError
from ...models import Recipe, RecipeNutrient


class Command(BaseCommand):

    def parse_value(self, value):
        return re.findall(r'[-+]?\d+\.\d+|\d+', value)

    def calculate_food_richness(self, value, num):
        return float(self.parse_value(value)[0]) / num

    def change_nfr(self):

        recipes = Recipe.objects.all()

        for recipe in recipes:

            lim3 = 0; nr = 0; nrf = 0
            sugars = 0; sodium = 0; sat_fat = 0
            protein = 0; fiber = 0; calcium = 0; 
            potassium = 0; magnesium = 0; iron = 0; 
            vitaminA = 0; vitaminC = 0; thiamin = 0; 
            niacin = 0; vitaminB6 = 0; folate = 0
            
            nutrients = RecipeNutrient.objects.filter(recipe=recipe)

            for nutrient in nutrients:
                name = str(nutrient.nutrient)
                value = nutrient.nutrient_value

                if name == 'Total Fat':
                    pass
                elif name == 'Total Carbohydrates':
                    pass
                elif name == 'Protein':
                    protein = self.calculate_food_richness(value, 57)
                elif name == 'Dietary Fiber':
                    #Fiber
                    fiber = self.calculate_food_richness(value, 25)
                elif name == 'Calcium':
                    #Calcium
                    calcium = self.calculate_food_richness(value, 800)
                elif name == 'Potassium':
                    #Potassium
                    potassium = self.calculate_food_richness(value, 2000)
                elif name == 'Magnesium':
                    #Magnesium
                    magnesium = self.calculate_food_richness(value, 375)
                elif name == 'Iron':
                    #Iron
                    iron = self.calculate_food_richness(value, 14)
                elif name == 'Vitamin A':
                    #Vitamin A
                    vitaminA = self.calculate_food_richness(value, 800)
                elif name == 'Vitamin C':
                    #Vitamin C
                    vitaminC = self.calculate_food_richness(value, 80)
                elif name == 'Thiamin':
                    #Vitamin B1
                    thiamin = self.calculate_food_richness(value, 1.1)
                elif name == 'Niacin':
                    #Vitamin B3
                    niacin = self.calculate_food_richness(value, 16)
                elif name == 'Vitamin B6':
                    #Vitamin B6
                    vitaminB6 = self.calculate_food_richness(value, 1.4)
                elif name == 'Folate':
                    #Folate
                    folate = self.calculate_food_richness(value, 200)
                elif name == 'Saturated Fat':
                    sat_fat = self.calculate_food_richness(value, 20)
                elif name == 'Sugars':
                    #Sugars
                    sugars = self.calculate_food_richness(value, 90)
                elif name == 'Sodium':
                    #Sodium
                    sodium = self.calculate_food_richness(value, 2400)
                elif name == 'Cholesterol':
                    pass
                else:
                    print("Not found", name)

            nr = protein + fiber + calcium + potassium + magnesium +\
                iron + vitaminA + vitaminC + thiamin + niacin +\
                vitaminB6 + folate
            lim3 = sat_fat + sodium + sugars
            nrf = nr - lim3

            print("Old value", recipe.nrf)
            recipe.nr = nr
            recipe.lim3 = lim3
            recipe.nrf = nrf
            recipe.save()
            print("New value", recipe.nrf)
            print("\n")

    def handle(self, *args, **options):
        self.change_nfr()
