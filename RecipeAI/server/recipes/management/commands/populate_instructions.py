'''
Script for populating instructions and recipe_instructions
!precondition recipes
'''
import os
import sys
import json
from django.core.management.base import BaseCommand, CommandError
from ...models import Recipe, Instruction, RecipeInstruction
from os import listdir
from os.path import isfile, join
import logging

logger = logging.getLogger('recipes')
logger.setLevel(logging.ERROR)


''' file_path '''
module_dir = os.path.dirname(__file__)
PROJECT_ROOT = os.path.realpath(os.path.dirname(module_dir))
DIR_PATH = os.path.join(PROJECT_ROOT, 'commands/DBManagement/data')
JSONFILES_FOLDER = [os.path.join(PROJECT_ROOT, 'commands/DBManagement/data/' + f)\
 for f in listdir(DIR_PATH) if isfile(join(DIR_PATH, f))]

class Command(BaseCommand):

    '''
    adding recipe instruction to database - recipe instruction
    is many to many connection table between recipe and instruction
    '''
    def add_recipe_instruction(self, recipe, instruction, step):
        try:
            instruction = Instruction.objects.get(instruction=instruction)
            recipe = Recipe.objects.get(recipe=recipe)
            RecipeInstruction.objects.get_or_create(recipe=recipe,\
                                                    instruction=instruction,\
                                                    step=step)
        except Exception as e:
            logger.error('Exception message '+ str(e))

    '''
    adding instruction to database
    @param recipe is recipe name   
    @param instructions are recipe instructions  
    @param step is step of instruction
    '''
    def add_instruction(self, recipe, instruction, step):
        try:
            try:
                Instruction.objects.get_or_create(instruction=instruction)
            except:
                print("Error while inserting instruction data")

            self.add_recipe_instruction(recipe=recipe,\
                                        instruction=instruction,\
                                        step=step)                                    
        except Exception as e:
            print("Error while adding instruction" + str(e))
            

    
    '''
    @param recipe is recipe name   
    @param instructions are recipe instructions
    '''    
    def populate(self, recipe, instructions):
        step = 1
        
        for instruction in instructions:
            """remove empty instructions and don't add step"""
            if instruction == "":
                continue
            """remove multiple spaces"""
            instruction = ' '.join(instruction.split())
            self.add_instruction(recipe=recipe, instruction=instruction, step=step)
            step += 1


    
    help = 'Populating instructions database'

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Successfully called script populate instructions'))

        for file_path in JSONFILES_FOLDER:
            self.stdout.write(self.style.WARNING(file_path))
            with open (file_path) as f:
                data = json.load(f)

            for recipe in data:
                self.populate(recipe=recipe['name'], instructions=recipe['instructions'])
        
        
        

