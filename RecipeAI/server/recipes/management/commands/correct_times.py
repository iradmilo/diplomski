import os
import sys
import json
import re
from django.db import IntegrityError
from pprint import pprint
from django.core.management.base import BaseCommand, CommandError
from ...models import Recipe
from os import listdir
from os.path import isfile, join
from django.db.utils import OperationalError

module_dir = os.path.dirname(__file__)
PROJECT_ROOT = os.path.realpath(os.path.dirname(module_dir))
DIR_PATH = os.path.join(PROJECT_ROOT, 'commands/DBManagement/data')
JSONFILES_FOLDER = os.path.join(PROJECT_ROOT, 'commands/DBManagement/data/recipe-times-corrected.json')

class Command(BaseCommand):

    def correct_times(self, data):
        counter = 0

        for recipe in data:

            name = recipe['name']

            prep_time = recipe['prep_time']

            cook_time = recipe['cook_time']

            try:
                recipe = Recipe.objects.filter(recipe=name).update(prep_time=prep_time, cook_time=cook_time)
                counter += 1
            except (Recipe.DoesNotExist, OperationalError) as dne:
                print(dne)

        print(counter)

    help = 'Correcting prep and cook times'

    def handle(self, *args, **options):

        self.stdout.write(self.style.SUCCESS('Successfully called script correct_times'))

        
        with open (JSONFILES_FOLDER) as f:
            data = json.load(f)
            

        self.correct_times(data)