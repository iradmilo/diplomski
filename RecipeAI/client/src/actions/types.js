export const FETCH_INGREDIENTS = 'fetch_ingredients';
export const FETCH_RECIPES = 'fetch_recipes';
export const SORT_RECIPES = 'sort_recipes';
export const EMPTY_RECIPES = 'empty_recipes'