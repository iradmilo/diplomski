import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'actions';
import _ from 'lodash';
import { Doughnut, Bar } from 'react-chartjs-2';
import { Preloader, Collection, CollectionItem } from 'react-materialize';
import './css/Recipe.css';
import Scroll from './Scroll';
import NutrientsModal from './NutrientsModal';

class Recipe extends Component {

  constructor() {
    super();

    this.state = {
      selectedIngredient: [],
      category: "All"
    };
  }

  componentWillMount() {  
    const selectedIngredient = localStorage.getItem('selectedIngredients');
    const category = localStorage.getItem('category');

    if (selectedIngredient) {
      this.setState({ selectedIngredient: JSON.parse(selectedIngredient) });
    }
    
    if (category) {
      this.setState({ category: JSON.parse(category) });
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    if(this.props.recipe === "No data")
      this.fetchRecipes()
  }

  fetchRecipes(){
    if (Array.isArray(this.state.selectedIngredient) && this.state.selectedIngredient.length !== 0){
      this.props.fetchRecipes(this.state.selectedIngredient, this.state.category, this.props.history)
    }
    else this.props.history.push('/search')
  }

  returnBarChartData(){
    return {
      labels: [
        'NRF (Nutrient Rich Food Score)',
        'NR (Nutrient Rich Score)',
        'LIM3 (Limit score)'
      ],
      datasets: [{
        label: 'Nutrient Rich Recipe Data',
        data: [
          this.props.recipe.nrf,
          this.props.recipe.nr,
          this.props.recipe.lim3
        ],
        backgroundColor: [
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(255, 99, 132, 0.2)'
        ],
        borderColor: [
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(255,99,132,1)'
        ],
        borderWidth: 1
      }]  
    }
  }

  returnDoughnutChartData(){
    var total_per = this.props.recipe.protein_per +
    this.props.recipe.carb_per + 
    this.props.recipe.fat_per
    var rest = 0
    if(total_per < 100)
      rest = 100 - total_per
    return {
      datasets: [{
        data: [
          this.props.recipe.protein_per,
          this.props.recipe.carb_per,
          this.props.recipe.fat_per,
          rest
        ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(153, 102, 255, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(153, 102, 255, 1)'
        ],
        borderWidth: 1
      }],
      labels: [
        'Protein',
        'Carbohydrates',
        'Fat',
        'Other'
      ]
    }
  }

  highlightSearchedIngredientsRegExp(ingredient){
    for(var search of this.props.recipe.ingredients_in_recipe){ 
      var regexstring = '\\b'+search+'(es|s)?\\b'
      var re = new RegExp(regexstring, "gi")
      var array = re.exec(ingredient)
      if(array){
        var index=array['index']
        var lastIndex=index+search.length
        return <span key={ingredient}>
          {ingredient.slice(0, index)}
          <span className="highlight">
          {ingredient.slice(index, lastIndex)}</span>{ingredient.slice(lastIndex)} </span>;
      } 
    }
    return ingredient;
  }

  renderIngredients(){
    return _.map(this.props.recipe.ingredient_list, ingredient => {
      return(
        <div key={ingredient}>
          {this.highlightSearchedIngredientsRegExp(ingredient)}
        </div>
      )
    })
  }

  renderSearchedIngredients(){
    return _.map(this.props.recipe.ingredients_in_recipe, ingredient => {
      return(
        <div key={ingredient}>
          {ingredient}
        </div>
      )
    })
  }

  renderInstructionDivPadding(instruction, padding){
    return(
      <span style={{ 'paddingTop': padding }}>
        {instruction}
      </span>
    )
  }

  renderInstructions(){
    return _.map(this.props.recipe.instructions, (instruction, index) => {
      return(
        <div key={instruction} style={{'marginBottom':'5px'}}>
          <span style={{ 'display': 'flex' }}> 
            <span style={{ 'flexShrink': '0',
                            'display': 'block',
                            'width': '50px' }}>            
              <span className="numberCircle"><span>{index}</span></span>
            </span>
            {index < 10 ? this.renderInstructionDivPadding(instruction, '2px') :
              this.renderInstructionDivPadding(instruction, '6px')
            }
          </span>
        </div>
      )
    })
  }

  renderSpan(prop, data){
    return(
      <span style={{ 'display': 'flex', 'textAlign': 'right' }}> 
        <span style={{ 'flexShrink': '0',
                        'display': 'block',
                        'width': '40px' }}>            
          <span>{prop}</span>
        </span>
        <span>
          {data}
        </span>
      </span>
    );
  }

  renderNutrientInfoCollectionItems(){
    return(
      <div style={{'marginTop': '30px'}}>
        <Collection className="row">
          <CollectionItem key={this.props.recipe.nrf} style={{ 'padding': '0px 20px' }}>
            <div style={{ 'paddingLeft': '10px', 'paddingTop': '15px' }} className="row">
              <div className="left col s6" style={{ 'marginTop': '5px' }}>
                Nutrient Rich Food Score
              </div>
              <div className="right col s6" style={{ 'marginTop': '5px', 'textAlign': 'right' }}> 
                {this.props.recipe.nrf.toFixed(5)}    
              </div>
            </div>
          </CollectionItem>
          <CollectionItem key={this.props.recipe.nr} style={{ 'padding': '0px 20px' }}>
            <div style={{ 'paddingLeft': '10px', 'paddingTop': '15px' }} className="row">
              <div className="left col s6" style={{ 'marginTop': '5px' }}>
                Nutrient Rich Score
              </div>
              <div className="right col s6" style={{ 'marginTop': '5px', 'textAlign': 'right' }}> 
                {this.props.recipe.nr.toFixed(5)}    
              </div>
            </div>
          </CollectionItem>
          <CollectionItem key={this.props.recipe.lim3} style={{ 'padding': '0px 20px' }}>
            <div style={{ 'paddingLeft': '10px', 'paddingTop': '15px' }} className="row">
              <div className="left col s6" style={{ 'marginTop': '5px'}}>
                Limit 3 Nutrients Score
              </div>
              <div className="right col s6" style={{ 'marginTop': '5px', 'textAlign': 'right' }}> 
                {this.props.recipe.lim3.toFixed(5)}    
              </div>
            </div>
          </CollectionItem>
        </Collection>
      </div>
    )
  }

  renderRecipeTimeInfo(prop){
    let info = ''
    let time = ''
    switch(prop){
      case "prep":
        info="Prep"
        time=this.props.recipe.prep_time
        break;
      case "cook":
        info="Cook"
        time=this.props.recipe.cook_time
        break;
      case "ready":
        info="Ready"
        time=this.props.recipe.ready_time
        break;
      default:
        return;
    }
    return(
      <span style={{ 'display': 'flex' }}> 
        <span style={{ 'flexShrink': '0',
                        'display': 'block',
                        'width': '50px' }}>            
          <span>{info}</span>
        </span>
        <span>
          {time}
        </span>
      </span>
    )
  }

  //function for coloring macronutrient balance text, yellow if out green in recommended values
  returnPerClassName(nutrient_type_per, min, max){
    return `right col s6 ${(nutrient_type_per >= min && nutrient_type_per <= max) ? "green-text" : ""}`
  }

  //Percentage of protein, fat and carbs
  renderPercentageInfoCollectionItems(){
    let total = this.props.recipe.total_per
    if(total <= 100)
      total = 100
    let other = total - 
                (this.props.recipe.protein_per + 
                 this.props.recipe.carb_per + 
                 this.props.recipe.fat_per)
    return(
      <Collection className="row">
        <CollectionItem key={Math.random()} style={{ 'padding': '0px 20px' }}>
          <div style={{ 'paddingLeft': '10px', 'paddingTop': '15px' }} className="row">
            <div className="left col s6" style={{ 'marginTop': '5px' }}>
              Protein percentage
            </div>
            <div className={this.returnPerClassName(this.props.recipe.protein_per, 10, 35)}
                 style={{ 'marginTop': '5px', 'textAlign': 'right' }}> 
            {this.props.recipe.protein_per}%
            </div>           
          </div>
        </CollectionItem>
        <CollectionItem key={Math.random()} style={{ 'padding': '0px 20px' }}>
          <div style={{ 'paddingLeft': '10px', 'paddingTop': '15px' }} className="row">
            <div className="left col s6" style={{ 'marginTop': '5px' }}>
              Carbohydrates percentage
            </div>
            <div className={this.returnPerClassName(this.props.recipe.carb_per, 45, 65)}
                 style={{ 'marginTop': '5px', 'textAlign': 'right' }}> 
              {this.props.recipe.carb_per}%
            </div>
          </div>
        </CollectionItem>
        <CollectionItem key={Math.random()} style={{ 'padding': '0px 20px' }}>
          <div style={{ 'paddingLeft': '10px', 'paddingTop': '15px' }} className="row">
            <div className="left col s6" style={{ 'marginTop': '5px'}}>
              Fat percentage
            </div>
            <div className={this.returnPerClassName(this.props.recipe.fat_per, 25, 35)} 
                 style={{ 'marginTop': '5px', 'textAlign': 'right' }}> 
              {this.props.recipe.fat_per}%
            </div>
          </div>
        </CollectionItem>
        <CollectionItem key={other+Math.random()} style={{ 'padding': '0px 20px' }}>
          <div style={{ 'paddingLeft': '10px', 'paddingTop': '15px' }} className="row">
            <div className="left col s6" style={{ 'marginTop': '5px'}}>
              Rest
            </div>
            <div className="right col s6" style={{ 'marginTop': '5px', 'textAlign': 'right' }}> 
              {other}%
            </div>
          </div>
        </CollectionItem>
      </Collection>
    )
  }

  renderRecipeInfo(){
    return(
      <div>
        <h4 className="center" style={{ 'paddingTop':'30px' }}> 
          {this.props.recipe.recipe}
        </h4>
        <div id="g-info" className="recipe_look section scrollspy"> 
          General info
        </div>
        <div>
          <div>
            Category: {this.props.recipe.category}
          </div>
          <div>
            {`Score:  ${this.props.recipe.score.toFixed(5)}`}
          </div>
          <div>
            {`Servings:  ${this.props.recipe.servings}`}
          </div>
          <div>
            {`Calories:  ${this.props.recipe.calories}`}
          </div>
          <a href={this.props.recipe.url}> {this.props.recipe.url} </a>
        </div>
        <div id="s-ingredients" className="recipe_look section scrollspy">
          Searched ingredients
        </div>
        <div>
          {this.renderSearchedIngredients()}
        </div>
        <div id="ingredients" className="recipe_look section scrollspy">
          Ingredients
        </div>
        <div>
          {this.renderIngredients()}
        </div>
        <div id="instructions" className="recipe_look section scrollspy">
          Instructions
        </div>
        <div className="row" style={{ 'marginTop': '10px' }}>
          <div className="left col s6" style={{'marginBottom':'10px'}}>
            <i className="material-icons">timer</i>
              {this.props.recipe.prep_time ? this.renderRecipeTimeInfo("prep") : ''}
              {this.props.recipe.cook_time ? this.renderRecipeTimeInfo("cook") : ''}
              {this.props.recipe.ready_time ? this.renderRecipeTimeInfo("ready") : ''}
          </div>
          <div className="col m12 l12 s12" style={{ 'marginTop': '20px' }}>
            {this.renderInstructions()}
          </div>
        </div>
        <div className="row">   
          <div className="left col l12 m12 s12" style={{ 'paddingTop': '30px', 'paddingBottom': '20px' }}>
            <div className="recipe_look center section scrollspy">
              Nutrient rich food index data
            </div >
            {this.renderNutrientInfoCollectionItems()}
            <div style={{ 'paddingLeft': '10px' }}>
              <blockquote className="flow-text" 
                          style={{ 'fontSize': '0.9em', 'marginBottom': '40px' }}>
                <span style={{'fontWeight':'bold'}}>Nutrient Rich Score </span> 
                is consisting of 12 recommended nutrients (
                Protein, Fiber, Calcium, Potassium, Magnesium, Iron, VitaminA, VitaminC,
                Thiamin, Niacin, VitaminB6, Folate)
                <br />
                <span style={{'fontWeight':'bold'}}>Limit 3 Score </span> is 
                consisting of 3 nutriets whose intake should be limited (
                Saturated fats, Sodium, Sugars)
                <br />
                <span style={{'fontWeight':'bold'}}>Nutrient Rich Food Score </span> is 
                calculated as Nutrient Rich Score - Limit 3 Score
                <br/>
              </blockquote>
            </div>
          </div>
          <div id="nrf">
            <h6 className="center">Nutrient Rich Food Index graph</h6>
            <div className="col l12 hide-on-small-only" 
                style={{ 'paddingTop': '10px', 'width': '100%', 'height': '100%', 'marginBottom': '60px' }}>
              <Bar data={this.returnBarChartData()}
                  options={{
                    maintainAspectRatio: false
                  }}/>
            </div>
          </div>
          <div className="row">
            <div id="n-per" className="recipe_look center section scrollspy">
              Nutrient percentages
            </div>
            <div className="left col l6 m6 s12" 
                style={{ 'paddingTop': '20px' }}>
                {this.renderPercentageInfoCollectionItems()}
            </div>
            <div className="right col l6 m6 hide-on-small-only" 
                style={{ 'paddingTop': '20px', 'height': '300px', 'width': '300px'}}>
              <Doughnut data={this.returnDoughnutChartData()}
                        width={100}
                        height={100}
                        options={{
                          maintainAspectRatio: false
                        }}/>
            </div>
            <div className="border-bottom center col l12 s12 m12"
                 style={{ 'paddingTop': '20px' }}> 
              Macronutrient balance score: {this.props.recipe.macronutrient_balance}
              <p className="valign-wrapper"
                 style={{ 'paddingLeft':'10px', 'paddingTop': '10px' }}>
                <i className="material-icons">info</i> 
                Macronutrient balance: Proteins: 10-35% total kcal, Carbohydrates: 45-65% total kcal, Fats: 25-35% total kcal       
              </p> 
            </div>
          </div>
          <div className="row">
              <div id="n-info" className="recipe_look center section scrollspy">
                Nutritional information
              </div>
              {this.props.recipe.nutrients.length !== 0 ?
                <div className="left">
                  {this.props.recipe.nutrients.length !== 0}
                  {`Per Serving: ${this.props.recipe.calories} calories;
                  ${this.props.recipe.nutrients[0].values.value} fat; 
                  ${this.props.recipe.nutrients[5].values.value} carbohydrates; 
                  ${this.props.recipe.nutrients[7].values.value} protein; 
                  ${this.props.recipe.nutrients[2].values.value} cholesterol; 
                  ${this.props.recipe.nutrients[3].values.value} sodium`}
                </div> :
                <div className="left">
                  No nutritional data for this recipe
                </div>
              }
              <div className="right" style={{ 'marginTop': '10px' }}>
                <NutrientsModal recipe={this.props.recipe} />
              </div>
          </div>
        </div>
      </div>
    )  
  }

  renderPreloading(){
    return(
      <div className="center">
        <Preloader flashing/>
      </div>
    )
  }

  redirectToSearch(){
    this.props.history.push(`/search`)
  }

  render(){
    if(this.props.recipes.length !== 0 && this.props.recipe === "No data")
      return(
        <div className="row" style={{ 'margin': '40px 0px' }}>
          <div className="center">
            No recipe fetched with specific name provided as a part of url 
            <br />
            Redirecting you to search
            <span style={{ 'display': 'None'}}> {setTimeout(() => this.redirectToSearch(), 1500)} </span>
          </div>
        </div>
      )
    return(
      <div className="row" style={{ 'margin': '20px 0px' }}>
        <Scroll />
        {this.props.recipe !== "No data" ? this.renderRecipeInfo() : this.renderPreloading()}
      </div>
    )
  }
}

function mapStateToProps({ recipes }, ownProps) {
  let searchedRecipe = _.find(recipes['recipes'], (recipe) => {
    return recipe.recipe === ownProps.match.params.name;
  })
  if(searchedRecipe !== undefined)
    return { recipes: recipes['recipes'], recipe: searchedRecipe };
  else
    return { recipes: recipes['recipes'], recipe: "No data" }
}
    
export default connect(mapStateToProps, actions)(Recipe);