import React from 'react';

export default () => {
  return(
    <footer style={{ 'marginTop': '20px' }}
            className="page-footer amber accent-1">
      <div className="container">
        <div className="row">
          <div className="col l6 s12">
            <h5 className="black-text">Recipe Picker</h5>
            <p className="grey-text text-darken-4">Recipes and ingredients info taken from links on the right</p>
          </div>
          <div className="col l4 offset-l2 s12">
            <h5 className="black-text">Links</h5>
            <ul>
              <li><a className="grey-text text-darken-1" href="https://www.allrecipes.com/">Allrecipes.com</a></li>
              <li><a className="grey-text text-darken-1" href="http://foodb.ca/">FoodDB</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div className="footer-copyright">
        <div className="container black-text">
          &copy; 2018 Copyright Recipe Picker - Site not intended for commercial use
          <a className="grey-text text-darken-4 right" href="https://www.fesb.unist.hr/">fesb</a>
        </div>
      </div>
    </footer>
  
  )
};