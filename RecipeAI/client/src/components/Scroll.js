import React from 'react';

const Scroll = () => {
  return(
    <div className="col hide-on-small-only m3 l2"
        style={{ 'position':'fixed',
        'right':'3%',
        'top':'10%',
        'width': '150px',
        'textAlign': 'right' }}>
      <ul className="section table-of-contents">
        <li><a href="#g-info">General info</a></li>
        <li><a href="#ingredients">Ingredients</a></li>
        <li><a href="#instructions">Instructions</a></li>
        <li><a href="#nrf">NRF Data</a></li>
        <li><a href="#n-info">Nutritional Info</a></li>
      </ul>
    </div>
  );
};

export default Scroll;