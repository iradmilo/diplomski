import React from 'react';
import { Link } from 'react-router-dom'

const Home = () => {
  return (
    <div style={{ textAlign: 'center', 'marginBottom': '40px' }}>
      <h1>
        Recipe Picker Web App!
      </h1>
      <p>  This app is practical part of my university master thesis!!! </p>
      <Link to={`/search`}>
        To search for recipes click here
      </Link>
    </div>
  );
};

export default Home;
