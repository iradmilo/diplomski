import React from 'react';
import _ from 'lodash';
import { Modal, Button } from 'react-materialize';

const renderNutrients = (recipe) => {
  return (
    <div style={{ 'padding': '4px 8px 12px', 'border': '1px solid #2d2d2d' }}>
      <div>
        <h4 style={{ 'fontWeight': 'bold' }}>Nutritional information</h4>
      </div>
      <div>
        <h5>{recipe.recipe}</h5>
      </div>
      <div style={{ 'padding': '5px 0px' }}>
        {`Servings per recipe: ${recipe.servings}`}
        <br/>
        <span style={{ 'fontWeight': 'bold' }}> {`Calories per serving: ${recipe.calories}`} </span>
        <br/>
      </div>
      <div style={{ 'borderTop': '1px solid #cfcfcf',
                    'textAlign': 'right',
                    'padding': '4px 0',
                    'borderBottom': '3px solid #2d2d2d'
                  }}>
        % Daily value *
      </div>
      <div style={{ 'paddingTop': '10px',
                    'borderBottom': '3px solid #2d2d2d'
                  }}>
        {_.map(recipe.nutrients, (nutrient) => {
          let moveLeft = '0px'
          switch (nutrient.nutrient){
            case "Saturated Fat":
            case "Dietary Fiber":
              moveLeft='20px';
              break;
            default:
              moveLeft = '0px';
          }
          return(
            <div className="row"
                style={{ 'borderBottom': '1px solid #cfcfcf',
                         'marginBottom': '5px' }}
                key={Math.random()}>
              <div className="left col-s6 small"
                  style={{ 'paddingLeft': moveLeft,
                           'fontWeight': 'bold' }}>
                {`${nutrient.nutrient}:   `}
                <span style={{ 'fontWeight': 'normal' }}>{`${nutrient.values.value ? nutrient.values.value : ''}`}</span>
              </div>
              <div className="right col-s6 small">
                {`${nutrient.values.daily_value ? `${nutrient.values.daily_value} %` : ''}`}
              </div>
            </div>
          );
        })}
        </div>
        <div style={{ 'paddingTop': '10px' }}>
          <div style={{ 'display': 'flex' }}>
            <span style={{ 'flexShrink': '0',
                           'display': 'block',
                           'width': '15px' }}>
                           *
                           </span>
            <span>Percent Daily Values are based on a 2,000 calorie diet. 
             Your daily values may be higher or lower depending on your calorie needs.</span>
          </div>
          <div style={{ 'display': 'flex' }}>
            <span style={{ 'flexShrink': '0',
                           'display': 'block',
                           'width': '15px' }}>
                           **
                           </span>
            <span>Nutrient information is not available for all ingredients. 
              Amount is based on available nutrient data.</span>
          </div>
          <div style={{ 'display': 'flex' }}>
            <span style={{ 'flexShrink': '0',
                           'display': 'block',
                           'width': '15px' }}>
                           (-)
                           </span>
            <span>Information is not currently available for this nutrient. 
              If you are following a medically restrictive diet, 
              please consult your doctor or registered dietitian before preparing this recipe for personal consumption.</span>
          </div>
          <p style={{ 'fontSize': '.875rem',
                      'fontWeight': 'bold',
                      'marginBottom': '4px',
                      'paddingTop': '8px',
                      'paddingLeft': '16px',
                      'paddingRight': '16px',
                      'width': '100%',}}>
            Powered by the ESHA Research Database &copy; 2018, 
            <a href="https://www.esha.com/"> ESHA Research, Inc. </a>
            All Rights Reserved
          </p>
        </div>
    </div>
  )
};


const NutrientsModal = ({recipe}) => {
  return (
    <Modal
      trigger={<Button 
                className="waves-effect waves-light btn-small right amber accent-1 black-text"
                style={{ 'marginBottom': '30px', textAlign: 'center' }}>
                  Nutrients
                  <i className="material-icons right">info</i>
                </Button>}>
      {renderNutrients(recipe)}
    </Modal>
  );
};

export default NutrientsModal;